It is a wonderful world?
It is a wonderful world!
I love the world, I love my life.
Well, there is indeed cruelty, brutality, war, earthquake, inundation and pollution, but far away.
And if I am honest to myself, if we are honest to ourselves we do not care about the war in former Yugoslavia, we do not care about the starvation in Africa and Asia, we do not care about the starvation in Africa and Asia, we do not care about the ozone hole in Australia.
These problems are of course serious and sometimes shocking but the only thing I am able to do and I want to do is to give money to Caritas or Red Cross.
So for me it is a wonderful world.
I enjoy all days of my life –  the bad ones and the good ones.  said "carpe diem" – you have to be able to enjoy and to utilize also sad situations and bad experiences in our life.  I enjoy sun as well as rain, I enjoy victories as well as defeats, I enjoy love as well as heartache.
Well, these things are normal and happen every day, happen in daily life.
And then there are some events which bring again and again into my mind that it is a really wonderful world.
For example meetings with some good old friends, journeys through Europe, a walk through the city of, hikes over the Austrian mountains, no matter if the sun is shining or if it is raining.
Or, for another example, the great success of the . soccer team.
And if there are days when I wish I was dead, no matter, better days will come and thank Lord for these experience, for these feelings of disappointment and grief.
Of course there are norms, rules and standards in daily life of our society which are important for living and which I accept and follow and sanction, but first "what is society" and second "I have only one life – I have to enjoy it as well as I can".
