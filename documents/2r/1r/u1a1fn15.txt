﻿Communication between people has always been a crucial matter. We have got our own human language thanks to which we can express our thoughts. No wonder that there are so many possible ways in which we can communicate starting from writing letters, watching TV, and ending with talking on the phone. Undoubtedly telephone is the easier and the quickest means of communication.

The last invention in the telecommunication field is the mobile phone. Like every technological invention cellular phones have vices and virtues. But these are positive sides that I am going to write about.

First of all, this kind of a telephone is mobile, which means that one can carry it wherever he or she wants to. There are no wires which very often limit our natural movements so one can walk or even jump while talking.

Secondly, this phone is a very handy one. Even ladies can put this phone into their tiny handbags without any problems.

Moreover, mobile phones are more complex than stational ones. There are many options which you can choose from, so not only can you talk with somebody but also play games or look in your private yellow pages. The newest models now, have even little scenes that are installed in your phone and thus you not only hear but also see your speaker.

The last but not the least advantage is that the mobile phones are less noisy. One can set the level of the ring strength or switch it off  when unnecessary.

Being aware of all above mentioned positive arguments, one should not be surprised that more and more people are becoming the owners of the mobile phones.


