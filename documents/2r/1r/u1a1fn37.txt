﻿Mobile phones can be regarded as a social phenomenon. Nowadays hardly any people do not possess them. This phenomenon crosses all social and class barriers, because even the poorest people are owners of at least one mobile phone. And now a question arises: are mobile phones our foes or , on the contrary, our friends?

From my point of view, those devices are mainly meant to contribute to the society 's benefit. Of course, these products of technology similarly to other things of this kind have got their pros and cons. However, I think  the positive sides of mobile phones outnumber the negative ones.

First of all, mobile phones enable us a constant contact (unless they are not turned on) with other people - our friends, family. Thanks to them we have this opportunity to fasten our relations with people we can not meet personally. Certainly, in the long run mobile phones can not be substitutes of normal contacts, however, it is commonly known that people of today 's world have less and less free time. Which tends to be filled also with everyday chores.

Moreover, mobile phones can be very helpful in many situations. Every day we witness a number of tragedies - people get killed in car accidents, die covered by thick snow of avalanches, tumble down from high rocks and so on. People who happen to be in this kind of dire straits are not necessarily doomed to death. Mobile phones can often contribute to their saving.

We should also take into account another positive side of these devices. Mobile phones have taken on a very important meaning in the business even on the global scale. Businessmen can not do without them, because by means of mobile phones they stay in touch with their customers and partners, and settle lots of matters without having to make a single move.

However, as lots of other products of technology they have some drawbacks. Recent researches conducted by medicians show that electromagnetic waves produced by mobile phones exert some kind of influence on the human brain and may cause a brain tumor.

Besides, people, especially those who overuse those products may go to dogs by high phone bills, which often are relatively high.

Thus, the home budget of theirs may get weaker and those people may not be able to afford other expensive.

Moreover, limiting contacts with people only to phone calls may lead to the deterioration  of our relations with them. Certainly, we could find more negative sides of mobile phones. Funnily enough, people seem not to discern them at all. From my point of view we should not exaggerate, because everything exerts an effect on our lives even though we seem not to be aware of this fact. Because I am also an owner of a mobile phone I can not say that I suffer from it. and so millions of other people world-wide who have decided to use it.

It is also difficult for me to perceive mobile phones as a blessing, because if it had not been invented, people would communicate in a different way now. Nevertheless, who knows for how long mobile phones will keep their today 's importance.


