﻿In the world of today mobile phones became an integral part of our reality. They are no longer something out of the extraordinary, they are just the basics, whether we like it or not. Naturally, some people (mobile-nuts) can not envisage their lives without their cellulars, others - just the opposite, the very ring of the phone makes them shiver and they deem mobiles as the scourge of our civilization.

Obviously, those who are bitter opponents of the rampant gadgets are right to some degree. First of all, there is no denying that the mobile acts like a bug inserted under your skin, as you are under permanent surveillance. All they long you are available (under the mobile) and imposed to all people who are constantly nagging you about something. Surely, you can always switch off the phone, yet then, what is the point of having it ?

What is more, mobiles are often misused. Nowadays even primary school students are deeply convinced that they can not do without a cellular, in which they are eagerly encored by their parents. In fact, however, it is just another gadget they can brag about and show off in front of their peers.

On the other hand, we can not let ourselves be carried away with the "anti-mobile propaganda" and fail to notice the blatant advantages of cellulars.

First of all, mobiles proved to be a real blessing in numerous accidents. They saved people 's lives, when they were stranded and lost in mountains after avalanches, or during berserk storms at the sea. Owing to cellulars the rescue team were able to localize the marooned and have them save and sound.

Moreover, as we live in the era of communication and information the mobile enables us to keep pace with the constantly changing world. Stock brokers have to know the quotes all over the globe, businessmen need to change contracts on the spur of the moment, and even the middle-of-the road man needs to send a trivial SMS to his girlfriends with some warm words.

On the whole, there is no definite answer whether mobiles are a curse or a blessing of our civilization it all depends on us and the way we use them. The golden mean rule to use them reasonably, but to overuse them, seems best to follow. Strangely enough, are day having a mobile can turn out to be a matter of life or death, so let us not frown upon those rampant gadgets.

