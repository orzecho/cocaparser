Cell phones may be considered either a blessing or a curse. Whichever way you may consider them depends on how dependent you are on them.

If you are the type of person who has no other phone other than a cell then it may be a curse to you. No matter what you do you just can not seem to get away from your phone. People are constantly calling you for just little things. When it comes down to you just wanting a day to yourself, you simply just can not. If you turn off your cell to get away from it, people are coming over worried about you.

Cell phones are everywhere, Malls, Parks, schools even movie theaters. What has this world come to. There are just places that you should not be allowed to have a cell phone on.

Granted that if you run into some troubles with the weather and road conditions cell phones are needed for this type of problem. By having a cell phone with when driving may prevent you from becoming stranded in the middle of nowhere.

A cell phone should only be used in a car when you are in trouble and not driving. Way too many accidents happen while people are carelessly driving down the road and chatting with their friends or business officials.

Now for those of you who use their cell phones for the purpose of saving on long-distance bills or for an emergency while on the road than a cell phone to you may be 
a blessing.

If you are stranded with car troubles and need help you have your cell phone there to call for help. By using your cell phone for you and not handing your number out to the world you can save money with your free minutes instead of running up you phone bill. By not letting anyone but you immediate family having your number you can get away from everything for a day and worry that someone is looking for you.

Cell phones do not need to be your only source of communication. You can have 
a cell phone that will benefit you when you are in trouble. A cell does not need to be used to help others track you down for the smallest thing that they could do themselves.

Why worry people when they can not find you within . minutes of trying. Go ahead and have a cell phone but be smart and use it for you and not others.


