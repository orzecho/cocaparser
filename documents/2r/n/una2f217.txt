﻿I believe cell phones are a necessity for people in our world today. They come in handy every day and are very resourceful. You can use cell phones on the road, at the office, at home, or anywhere where there is not a public phone available.

When driving back and forth from place to place there is not always a phone nearby in case of an emergency. By having a cell phone along you increase your safety. If the need should arise that you have to call someone for help, you have easy access to a phone.

Cell phones are also a great way of always being able to get a hold of someone. Most people have cell phones nowadays and it is a lot easier to catch them on their cell phones than on their home phones.

For teenagers these days, a cell phone is a must. You will not find many that do not have one. It used to be that having your own phone in your room was cool, but now owning a cell phone is "in". It is a great way to communicate with friends in order to make plans. With all the things teens have going on in their lives, how many times are you actually going to be able to call them at home without having to leave a message?

In my family almost everyone has a cell phone. My twin sister and I share one for when we are driving to school, to Sioux Falls when shopping, and to sporting events. I feel a lot more secure having the cell phone if something should ever happen to us while driving. We also use it to call home after playing tennis, basketball, or softball and tell our parents how we did if they were not able to make it to the games.

A few weeks ago there was a story on the news about a man who had attempted to kidnap a young lady one afternoon. Fortunately for the girl, she had her cell phone along and was able to call for help before anything happened to her. In this case, the cell phone was a life-saving device.

As with everything, there are a few downsides to owning a cell phone. There is the problem of always having to charge it, not having good reception, and the most obvious: the cost. However, these problems are only a small price to pay for an invention that makes life easier and safer.


