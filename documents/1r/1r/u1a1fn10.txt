﻿The . century is undoubtedly an age of great breakthroughs in the history of mankind. What characterizes the times we live in is, so called, highly advanced technology. The world around us is a kind of a havoc where people are constantly in a hurry. We need more and more smart devices to make our lives easier and let us work faster and more effective. The pressure to pander to the human 's need of comfortable life makes producers all over the world come out with new ideas all the time. A mobile phone is one of those ideas. 

There is no denying that a cell phone was constructed to help busy people to organize their lives. It is meant for those who travel a lot and are never at home. At the begging of its existence it was perceived as a kind of redundant luxury. However, today it is getting more and more popular. Mobile phones are quite widespread now and there is nothing shocking about the fact that people are talking by the phone in the street or in the bus.

Oddly enough, there are situation in your life when you simply would not do without a mobile phone. Sometimes it can even save your life. For instance, in the USA a lot of people were rescued from the ruins of World Trade Centre thanks to the signal of their cell phones.

Still, some people would say that a mobile phone is a culprit rather than blessing. It simply tends to be abused by many of us. We stopped to treat it as a tool needed for our work and started to think of it as of a gadget.

For example, when Christmas come children are presented with mobile phones instead of simple toys. What is more a lot of teenagers get addicted to it in the same way they used to be addicted to the telly or computer games. Besides it seems to interfere with the basic way of communication, that is talking, as it often happens that you mobile rings while you are having a chat.

However, the worst of on is that your mobile phone never lets you relax in peace. You are always available, nowhere safe and always "under the phone".

All in all, mobile phones can be either . century monsters and wrongdoers or a blessing in disguise. It all depends on how you use them.


