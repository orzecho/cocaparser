﻿Mobile phones have gained as many supporters as opposers from the very beginning, from the moment they occurred in our surroundings. Since then people have been continuously quarrelling about the usefulness of mobile phones. Both - the supporters and the opposers have had their point. In my opinion it is no use arguing any longer because mobile phones is something that already exists and we will not get rid of it now. The question is - do we really need mobile phones or are we just trying to keep up with the Jones? No doubt, for some people a mobile phone is just a piece of technic which makes them feel sicker, happier, better. It is supposed to be a sign of their being well-off.

Obviously, not everybody acts this way. Many people simply need a mobile phone and that is the only reason they have bought it. They can not imagine working without it. For them a mobile phone is definitely a blessing. From my own experience I know that a mobile phone has one important thing to offer - it gives me that feeling of being more secure. I know, that whenever I am in trouble I can at least let somebody know about it. It is also very helpful - when I happen to have a puncture, a crash or simply when I want to tell my mother I am going to be late.

There are numerous examples of usefulness of mobile phones but there are also some disadvantages of having them. For instance - why is it always so that a mobile phone is ringing while it is really the least wanted to be ringing? Sometimes it can really spoil nice atmosphere.

So, the choice is ours. My opinion is: a mobile phone is a real blessing but only if we remember to turn it off at the right time.


