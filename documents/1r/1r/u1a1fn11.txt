﻿It is difficult to say whether mobile phone is a blessing or a curse. Certainly, there are both advantages and disadvantages of this special kind of phone.

The first in favor of mobile phone is that it is extremely useful in emergencies. When somebody witnesses a crash or participates in any kind of dangerous event he can always call for help. There is a special number, exclusive for mobile phones, that connect him with police.

Another advantage is that a mobile phone can turn out unrivalled for mothers and fathers who worry about their children that live far away from home or commute to school. If a child does not come back home at the hour he or she used to, frantic with worry mother has an opportunity to get in touch with her beloved offspring and check whether something bad happened.

On the other hand it is getting more and more noticeable that mobile phone is being overused. Some people who  do not really need it seem to treat cellular phone as something extremely necessary in their daily life. It appears to  be rude for some people that during a face to face conversation from one of the speakers ' pocket rings a mobile phone in Russia 's anthem tune! It is even more rude when the possessor of that phone does not seem to be abashed at all and he/she starts another conversation with the speaker at the other end of the phone line.

Also, there are certain situations when telephone ringing is embarrassing, annoying or even making people feel disgust. Best examples are: at church, during some service or at philharmonic concert of a great pianist. We all know such events. It is really so demanding just to switch off the sound?

All in all, at the end of the . century mobile phone is very useful. But we all should know how to use it and perhaps how not to overuse it. For some of us possessing a cellular phone can really be a blessing and for others a curse. Discussing it will always be subjective and the mobile phone, as it is, will  always be questionable.

The second advantage is that mobile phone is necessary at some kind of jobs. For instance a journalist must always know what is going on, a businessman, a doctor and many other professions can exist better with the help of phone.


