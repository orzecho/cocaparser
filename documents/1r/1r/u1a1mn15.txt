﻿Mobile phones, once regarded as symbols of wealth and financial independence. Over the last years have become popular and are used by all social groups. Nowadays, even first grade students at elementary school possess them and psychologists have to tackle social problems connected with mobiles. One could say that the western civilization has become psychologically addicted to those devices, but it is worth to ask, whether mobile phones are a blessing or a curse of the end of the . century.

Firstly, one should consider unexpected emergency situations that can be encountered every day. Traffic collisions, criminal offences, accidents or even traffic jams on one 's way for an important meeting occur daily, and by means of a mobile phone which simplifies and accelerates the communication, can be easily sorted out.

On the other hand, mobile phones can be very annoying, especially in public places as cinemas, theatres or means of public transport. One found with his mobile ringing at an inconvenient time and in inconvenient place would usually meet with general disregard, and as social consciousness grows throughout the years, the tendency is to avoid such situations.

Mobile phones, especially among young, independent people, are frequently used for social purposes. What mainly contributes to that fact, are short text messages, which can be sent by means of a mobile just like ordinary letters.

Another advantage of a mobile phone is the ability to connect with the internet, and therefore the ability to obtain all the information available in the world wide web. By means of a mobile one can receive the weather forecast, daily meetings ' schedule sent by one 's secretary or even up-to-date news, which substitutes purchasing a newspaper.

Finally, mobiles are not considered anymore as tokens of financial wealth. They are inexpensive and available almost for everyone, and the benefits that flow from the fact that one possesses it, far extend its cost.

Taking all advantages, disadvantages under consideration, I think  that  mobile phones are a blessing of the end of the . century, but one should not addict to it completely and should leave it home from time to time, what I personally do quite often.
