﻿

Mobiles phones, a curse to the . Century, I think not. A mobile phone in many circumstances has opened up the communication in between many people these days. They also update our modern society. Of course they may have a few downfalls, but many do not have to be so effective. A blessing and brilliant idea mobile phones are.

Whether it is to talk to someone within the same town or someone within a different state, mobile phones can be put to use. This alone has allowed for the communication lines to be more open between friends, family, co-workers, & numerous others. They allow you to stay in touch while traveling and when you are away from home. If you are lost while in your vehicle, or if you need immediate information from someone; cell phones are there. If there has been an accident along the road in the middle of nowhere, cell phones are there.

With all the other inventions that are coming about in today 's society I would only believe that a cell phone is up-to-date and very useful. Even though it is something that came out in the . Century, year-olds find them convenient. They only help to update our fast paced society along with computers, fax machines, and overnight mail delivery. There are many demands today that include immediate or as soon as possible responses. Cell phones allow for quick communication anywhere.

Some people dwell on the bad things that come with mobile phones, such as driving & talking. Well, there are options; you have the option to talk & drive at the same time or to use a headpiece, or simply pull over. If mobile phones cause accidents, they can be prevented because you do have options. Mobile phones may all also not work, in all places but again you have options to get a different plan or different cell phone company. With everything, you will find negative points but with a mobile phone the positives outweigh the negative.

The idea of a mobile phone was a brilliant one because they make life easier, and in today 's world that is what everyone wants. From a big important CEO to a . year old grandma, the range of people that own a cell phone is so large that they have to be positive and a blessing. So many people would not use them if they were not.


