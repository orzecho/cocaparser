﻿

Cell phones, nature 's way of getting even with us. When one thinks of cell phones, they often think of people having conversation with one another. Sometimes the conversations are joyous ones, and other times of sad events. Without a doubt, cell phones have made communication between people more convenient. In fact, they may have made things too convenient.

Why bother going and talking with a person face to face? Now you can just whip out that "handy-dandy" cell phone and call them from anywhere. You do not even need to find a phone outlet! 

Now, let us take things one step further. Imagine a household; just a typical one with a father, mother, and child. In this household the father owns a cell phone as do his wife and child. On one evening the father is watching the TV downstairs, it is Monday and football is on. Meanwhile, upstairs the son decides to listen to some music. The music ends up being too loud and begins to interfere with the father 's TV viewing. At this point the father could get up, walk upstairs, and tell the son to turn his music down; they might even start a little conversation. But, instead the father gets out his trusty cell phone and proceeds to call the son 's cell and quickly tell him to turn down the music, a potential moment between father and son is lost forever. The cell phone has destroyed another face to face conversation.

Another side of the cell phone can be seen in modern driving. This more deadly side and claims many lives every year. It usually starts the same way, and over confident driver believes he or she can talk on the cell phone and handle the responsibility of driving at the same time. They pull out the cell phone, then look away from the road to dial the number, by taking their eyes off the road they are risking an accident. Once they have dialled the number they start talking. At first they seem to do a decent job of talking and driving. Speed limits are ignored, turns are made more sloppily, and the general safety of the driver decreases. Not every time someone talks on the cell phone while driving, does an accident occur, but the risk of one increases dramatically.

In the end you could call the cell phone a mixed blessing. It gives people the power to communicate with each other easily; but, with any power comes a responsibility. Once people learn to use their cell phones in a more responsible manner, they will become a great asset to us. Until then the cell phone will carry with it a potential for trouble.


