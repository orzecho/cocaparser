﻿

Throughout the past decade, cellular telephones have become quite popular. For most people who have phones, they are a device for communication. Cell phones are a mainstay in the briefcase of a traveling businessman. Without them, businessmen would find it hard to stay in contact with corporate headquarters. It also allows for immediate contact with clients and loved ones. For this purpose, cell phones are a blessing for the people in today 's world.

Cell phones are best way for people to get in immediate contact with each other. Although sometimes overused, cell phones allow adults to discuss who is going to pick up the kids from school, agree on who is going to . soccer game, and explain why one will be late for dinner. Cell phones allow teenagers to set up plans with their friends and allows them to tell their parents what time they will be getting home. Cell phones allow families to stay in touch while they are out of the house.

Are there downsides to cell phones? Of course, but that should not take away from their capabilities. Some people overuse their cell phones. One of my biggest pet peeves is driving home from the cities during rush hour and watching people talk away on their phones. However, I do not feel so mad about it when I see the same people on the ten o'clock news talking to reporters about the car accident they got in. I do not believe that cell phones are the cause of these accidents. I believe that it is the driver 's fault. It is not the alcohol 's fault when somebody is in a drunk-driving accident. It should not be the cell phone 's fault when somebody gets into a fender-bender because they could not keep their attention on the road. If people would use common sense while driving a car, there would not be these inane incidents.

Cell phones are the single greatest device for communication in the market today. Although they can be somewhat expensive, the rewards can far outweigh the costs. Cell phones are now internet and e-mail accessible. They can now contact anybody in the world. However, the single greatest benefit of a cell phone is safety. If you ever get injured while carrying a cell phone, it is the best device for getting help. It could be the difference between life and death.

If you are considering getting a cell phone, I would recommend getting it. It is the best communication tool on the planet. And, it could save your life.


