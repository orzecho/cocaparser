﻿A mobile phone is one of the most popular devices of our times. However, we all know that using it has its advantages and disadvantages at the same time.

It goes without saying that in many situations a mobile phone is really indispensable. It makes us feel more secure we can call an ambulance on the phone any time. No matter where we are able to get in touch with another person, and for many people, especially those who lead a hectic life, this is the most important advantage of a mobile phone. Using it is one of the easiest ways to communicate.

However, the truth is that mobile phones are still very expensive. If a person does not want to go bankrupt, she or he must know how to use it properly. However, many people become addicted to using it. A mobile phone slowly appears to be their best friend and they can not live without its company. These days we might hear a telephone ring almost everywhere. Many of us seem to forget that for example the theatre is not a proper place to do business on the phone.

I am sure that mobile phones might be useful. They, somehow, help us to organize our life. But only under one condition - every owner of a mobile phone must realize that this is not a toy.

