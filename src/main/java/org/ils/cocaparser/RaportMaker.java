package org.ils.cocaparser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.*;

public class RaportMaker {
	private Connection c;
    private Statement s;
	
	public RaportMaker(){
		this.c = null;
	    this.s = null;
	    try {
	         Class.forName("org.postgresql.Driver");
	         c = DriverManager
	            .getConnection("jdbc:postgresql://localhost:5432/nlp",
	            "orzecho", "lerchonarcom2");
	         c.setAutoCommit(false);
	      } catch (Exception e) {
	         e.printStackTrace();
	         System.err.println(e.getClass().getName()+": "+e.getMessage());
	         System.exit(0);	
	      }
	      System.out.println("Opened database successfully");
	      
	      
	}
	
	public static void main(String[] args) throws SQLException, IOException{
		generateTrigramRaport();
	}

	private static void generateBigramRaport() throws SQLException, IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		RaportMaker r = new RaportMaker();
		r.s = r.c.createStatement();
		r.s.executeUpdate("truncate documents_pending_lenko;");
		r.s.executeUpdate("insert into documents_pending_lenko select document from bigrams_types_lenko group by document;");
	    r.c.commit();
		r.s.close();
		
		List<String> documents = new ArrayList<>();
		r.s = r.c.createStatement();
		ResultSet rs = r.s.executeQuery("select document from documents_pending_lenko;");
		while(rs.next()){
			documents.add(rs.getString("document"));
		}
		int cellID;
		while(!documents.isEmpty()){
			rs = r.s.executeQuery("select document,w1,w2,w1_freq,w2_freq,in_coca,freq_text,freq_coca,mean_freq,pmi,tscore,non_pmi_c,low_pmi_c,med_pmi_c,high_pmi_c,non_t_c, low_t_c, med_t_c, high_t_c from bigrams_raport_lenko where document = '" + documents.get(0) +"';");
			XSSFSheet spreadsheet = workbook.createSheet(documents.get(0));
			int rowID = 0;
			cellID = 0;
			XSSFRow row = spreadsheet.createRow(rowID++);
			Cell cell = row.createCell(cellID++);
			cell.setCellValue("Document");
			cell = row.createCell(cellID++);
			cell.setCellValue("Word 1");
			cell = row.createCell(cellID++);
			cell.setCellValue("Word 2");
			cell = row.createCell(cellID++);
			cell.setCellValue("Word 1 frequency");
			cell = row.createCell(cellID++);
			cell.setCellValue("Word 2 frequency");
			cell = row.createCell(cellID++);
			cell.setCellValue("In coca?");
			cell = row.createCell(cellID++);
			cell.setCellValue("Frequency in text");
			cell = row.createCell(cellID++);
			cell.setCellValue("Frequency in COCA");
			cell = row.createCell(cellID++);
			cell.setCellValue("Mean frequency in COCA");
			cell = row.createCell(cellID++);
			cell.setCellValue("PMI");
			cell = row.createCell(cellID++);
			cell.setCellValue("t-score");
			cell = row.createCell(cellID++);
			cell.setCellValue("PMI < 3");
			cell = row.createCell(cellID++);
			cell.setCellValue("PMI in [3,5)");
			cell = row.createCell(cellID++);
			cell.setCellValue("PMI in [5,7)");
			cell = row.createCell(cellID++);
			cell.setCellValue("PMI >= 7");
			cell = row.createCell(cellID++);
			cell.setCellValue("t < 2");
			cell = row.createCell(cellID++);
			cell.setCellValue("t in [2,6)");
			cell = row.createCell(cellID++);
			cell.setCellValue("t in [6,10)");
			cell = row.createCell(cellID++);
			cell.setCellValue("t >= 10");
			
			while(rs.next()){
				cellID = 0;
				row = spreadsheet.createRow(rowID++);
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getString("document"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getString("w1"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getString("w2"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("w1_freq"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("w2_freq"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("in_coca"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("freq_text"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("freq_coca"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("mean_freq"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("pmi"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("tscore"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("non_pmi_c") == 1 ? "*" : "" );
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("low_pmi_c") == 1 ? "*" : "" );
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("med_pmi_c") == 1 ? "*" : "" );
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("high_pmi_c") == 1 ? "*" : "" );
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("non_t_c") == 1 ? "*" : "" );
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("low_t_c") == 1 ? "*" : "" );
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("med_t_c") == 1 ? "*" : "" );
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("high_t_c") == 1 ? "*" : "" );
			
			}
			
			r.s.executeUpdate("delete from documents_pending_lenko where document = '" + documents.get(0) + "';");
			r.c.commit();
			documents.remove(0);
			}
			
			String summative_query = "select brl.document, "
					+ "d.round_id as ri, "
					+ "d.group_id as gi, "
					+ "wtf.wtf as word_tokens, "
					+ "sum(freq_text) as bigram_tokens, "
					+ "count(*) as bigram_types, "
					+ "sum(freq_text*in_coca) as bic_tokens, "
					+ "sum(freq_text*in_coca)::double precision / sum(freq_text) as bic_tokens_p, "
					+ "sum(in_coca) as bic_types, "
					+ "sum(in_coca)::double precision / sum(1) as bic_types_p, "
					+ "sum(freq_text) - sum(freq_text*in_coca) as bnic_tokens, "
					+ "(sum(freq_text) - sum(freq_text*in_coca))::double precision / sum(freq_text) as bnic_tokens_p, "
					+ "count(*) - sum(in_coca) as bnic_types, "
					+ "(count(*) - sum(in_coca))::double precision / count(*) as bnic_types_p, "
					+ "sum(freq_text*mean_freq*in_coca)/sum(freq_text*in_coca) as mean_freq_tokens, "
					+ "sum(mean_freq*in_coca)/sum(in_coca) as mean_freq_types, "
					+ "sum(pmi*freq_text*in_coca)/sum(freq_text*in_coca)::double precision as mean_pmi_tokens, "
					+ "sum(pmi*in_coca)/sum(in_coca) as mean_pmi_types, "
					+ "sum(tscore*freq_text*in_coca)/sum(freq_text*in_coca)::double precision as mean_t_tokens, "
					+ "sum(tscore*in_coca)/sum(in_coca) as mean_t_types, "
					+ "sum(non_pmi_c*in_coca) as non_pmi_c, "
					+ "sum(low_pmi_c*in_coca) as low_pmi_c, "
					+ "sum(med_pmi_c*in_coca) as med_pmi_c, "
					+ "sum(high_pmi_c*in_coca) as high_pmi_c, "
					+ "sum(non_t_c*in_coca) as non_t_c, "
					+ "sum(low_t_c*in_coca) as low_t_c, "
					+ "sum(med_t_c*in_coca) as med_t_c, "
					+ "sum(high_t_c*in_coca) as high_t_c, "
					+ "sum(non_pmi_c*freq_text*in_coca) as non_pmi_c_tokens, "
					+ "sum(low_pmi_c*freq_text*in_coca) as low_pmi_c_tokens, "
					+ "sum(med_pmi_c*freq_text*in_coca) as med_pmi_c_tokens, "
					+ "sum(high_pmi_c*freq_text*in_coca) as high_pmi_c_tokens, "
					+ "sum(non_t_c*freq_text*in_coca) as non_t_c_tokens, "
					+ "sum(low_t_c*freq_text*in_coca) as low_t_c_tokens, "
					+ "sum(med_t_c*freq_text*in_coca) as med_t_c_tokens, "
					+ "sum(high_t_c*freq_text*in_coca) as high_t_c_tokens, "
					+ "sum(non_pmi_c*in_coca)::double precision / sum(in_coca) as non_pmi_c_p, "
					+ "sum(low_pmi_c*in_coca)::double precision / sum(in_coca) as low_pmi_c_p, "
					+ "sum(med_pmi_c*in_coca)::double precision / sum(in_coca) as med_pmi_c_p, "
					+ "sum(high_pmi_c*in_coca)::double precision / sum(in_coca) as high_pmi_c_p, "
					+ "sum(non_t_c*in_coca)::double precision / sum(in_coca) as non_t_c_p, "
					+ "sum(low_t_c*in_coca)::double precision / sum(in_coca) as low_t_c_p, "
					+ "sum(med_t_c*in_coca)::double precision / sum(in_coca) as med_t_c_p, "
					+ "sum(high_t_c*in_coca)::double precision / sum(in_coca) as high_t_c_p, "
					+ "sum(non_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as non_pmi_c_p_tokens, "
					+ "sum(low_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as low_pmi_c_p_tokens, "
					+ "sum(med_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as med_pmi_c_p_tokens, "
					+ "sum(high_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as high_pmi_c_p_tokens, "
					+ "sum(non_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as non_t_c_p_tokens, "
					+ "sum(low_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as low_t_c_p_tokens, "
					+ "sum(med_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as med_t_c_p_tokens, "
					+ "sum(high_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as high_t_c_p_tokens "
					+ "from bigrams_raport_lenko as brl inner join documents_lenko as d on d.document = brl.document  inner join word_token_frequency as wtf on wtf.d = brl.document group by brl.document, wtf.wtf, d.group_id, d.round_id";
			
			r.s = r.c.createStatement();
			rs = r.s.executeQuery(summative_query);
			XSSFSheet spreadsheet = workbook.createSheet("Summative");
			int rowID = 0;
			cellID = 0;
			XSSFRow row = spreadsheet.createRow(rowID++);
			Cell cell = row.createCell(cellID++);
			cell.setCellValue("Document");
			cell = row.createCell(cellID++);
			cell.setCellValue("Round ID");
			cell = row.createCell(cellID++);
			cell.setCellValue("Group ID");
			cell = row.createCell(cellID++);
			cell.setCellValue("Word tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Bigram tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Bigram types");
			cell = row.createCell(cellID++);
			cell.setCellValue("Bigrams in COCA tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Bigrams in COCA tokens %");
			cell = row.createCell(cellID++);
			cell.setCellValue("Bigrams in COCA types");
			cell = row.createCell(cellID++);
			cell.setCellValue("Bigrams in COCA types %");
			cell = row.createCell(cellID++);
			cell.setCellValue("Bigrams not in COCA tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Bigrams not in COCA tokens %");
			cell = row.createCell(cellID++);
			cell.setCellValue("Bigrams not in COCA types");
			cell = row.createCell(cellID++);
			cell.setCellValue("Bigrams not in COCA types %");
			cell = row.createCell(cellID++);
			cell.setCellValue("Mean frequency tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Mean frequency types");
			cell = row.createCell(cellID++);
			cell.setCellValue("Mean PMI tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Mean PMI types");
			cell = row.createCell(cellID++);
			cell.setCellValue("Mean t-score tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Mean t-score types");
			cell = row.createCell(cellID++);
			cell.setCellValue("Non PMI collocations");
			cell = row.createCell(cellID++);
			cell.setCellValue("Low PMI collocations");
			cell = row.createCell(cellID++);
			cell.setCellValue("Medium PMI collocations");
			cell = row.createCell(cellID++);
			cell.setCellValue("High PMI collocations");
			cell = row.createCell(cellID++);
			cell.setCellValue("Non t-score collocations");
			cell = row.createCell(cellID++);
			cell.setCellValue("Low t-score collocations");
			cell = row.createCell(cellID++);
			cell.setCellValue("Medium t-score collocations");
			cell = row.createCell(cellID++);
			cell.setCellValue("High t-score collocations");
			cell = row.createCell(cellID++);
			cell.setCellValue("Non PMI collocations %");
			cell = row.createCell(cellID++);
			cell.setCellValue("Low PMI collocations %");
			cell = row.createCell(cellID++);
			cell.setCellValue("Medium PMI collocations %");
			cell = row.createCell(cellID++);
			cell.setCellValue("High PMI collocations %");
			cell = row.createCell(cellID++);
			cell.setCellValue("Non t-score collocations %");
			cell = row.createCell(cellID++);
			cell.setCellValue("Low t-score collocations %");
			cell = row.createCell(cellID++);
			cell.setCellValue("Medium t-score collocations %");
			cell = row.createCell(cellID++);
			cell.setCellValue("High t-score collocations %");
			cell = row.createCell(cellID++);
			cell.setCellValue("Non PMI collocations tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Low PMI collocations tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Medium PMI collocations tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("High PMI collocations tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Non t-score collocations tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Low t-score collocations tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Medium t-score collocations tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("High t-score collocations tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Non PMI collocations % tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Low PMI collocations % tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Medium PMI collocations % tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("High PMI collocations % tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Non t-score collocations % tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Low t-score collocations % tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("Medium t-score collocations % tokens");
			cell = row.createCell(cellID++);
			cell.setCellValue("High t-score collocations % tokens");
			
			while(rs.next()){
				cellID = 0;
				row = spreadsheet.createRow(rowID++);
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getString("document"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("ri"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("gi"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("word_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("bigram_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("bigram_types"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("bic_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("bic_tokens_p"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("bic_types"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("bic_types_p"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("bnic_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("bnic_tokens_p"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("bnic_types"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("bnic_types_p"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("mean_freq_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("mean_freq_types"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("mean_pmi_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("mean_pmi_types"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("mean_t_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("mean_t_types"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("non_pmi_c"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("low_pmi_c"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("med_pmi_c"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("high_pmi_c"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("non_t_c"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("low_t_c"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("med_t_c"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("high_t_c"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("non_pmi_c_p"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("low_pmi_c_p"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("med_pmi_c_p"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("high_pmi_c_p"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("non_t_c_p"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("low_t_c_p"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("med_t_c_p"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("high_t_c_p"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("non_pmi_c_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("low_pmi_c_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("med_pmi_c_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("high_pmi_c_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("non_t_c_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("low_t_c_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("med_t_c_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("high_t_c_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("non_pmi_c_p_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("low_pmi_c_p_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("med_pmi_c_p_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("high_pmi_c_p_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("non_t_c_p_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("low_t_c_p_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("med_t_c_p_tokens"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("high_t_c_p_tokens"));
			}
			LocalDateTime ldt = LocalDateTime.now();
			FileOutputStream out = new FileOutputStream(new File("raport_" + ldt.format(DateTimeFormatter.BASIC_ISO_DATE) + ".xlsx"));
			workbook.write(out);
			out.close();
		
	}

	private static void generateTrigramRaport() throws SQLException, IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		RaportMaker r = new RaportMaker();
		r.s = r.c.createStatement();
		r.s.executeUpdate("truncate documents_pending_lenko;");
		r.s.executeUpdate("insert into documents_pending_lenko select document from trigrams_types_lenko group by document;");
	    r.c.commit();
		r.s.close();
		List<String> documents = new ArrayList<>();
		r.s = r.c.createStatement();
		ResultSet rs = r.s.executeQuery("select document from documents_pending_lenko;");
		while(rs.next()){
			documents.add(rs.getString("document"));
		}
		while(!documents.isEmpty()){
					String actualDocument;
					actualDocument = documents.get(0);
					documents.remove(0);
					System.out.println("Working on " + actualDocument);
						
					Statement s = r.c.createStatement();
					rs = s.executeQuery("select * from trigrams_raport_lenko where document = '" + actualDocument +"';");
					XSSFSheet spreadsheet = workbook.createSheet(actualDocument);
					int rowID = 0;
					int cellID = 0;
					XSSFRow row = spreadsheet.createRow(rowID++);
							Cell cell = row.createCell(cellID++);
							cell.setCellValue("Document");
							cell = row.createCell(cellID++);
							cell.setCellValue("Word 1");
							cell = row.createCell(cellID++);
							cell.setCellValue("Word 2");
							cell = row.createCell(cellID++);
							cell.setCellValue("Word 3");
							cell = row.createCell(cellID++);
							cell.setCellValue("Trigram frequency in text");
							cell = row.createCell(cellID++);
							cell.setCellValue("Word 1 frequency");
							cell = row.createCell(cellID++);
							cell.setCellValue("Word 2 frequency");
							cell = row.createCell(cellID++);
							cell.setCellValue("Word 3 frequency");
							cell = row.createCell(cellID++);
							cell.setCellValue("Bigram 1 frequency");
							cell = row.createCell(cellID++);
							cell.setCellValue("Bigram 2 frequency");
							cell = row.createCell(cellID++);
							cell.setCellValue("Trigram frequency");
							cell = row.createCell(cellID++);
							cell.setCellValue("In coca?");
							cell = row.createCell(cellID++);
							cell.setCellValue("Mean freq");
							cell = row.createCell(cellID++);
							cell.setCellValue("PMI");
							
							while(rs.next()){
								cellID = 0;
								row = spreadsheet.createRow(rowID++);
								cell = row.createCell(cellID++);
								cell.setCellValue(rs.getString("document"));
								cell = row.createCell(cellID++);
								cell.setCellValue(rs.getString("word1"));
								cell = row.createCell(cellID++);
								cell.setCellValue(rs.getString("word2"));
								cell = row.createCell(cellID++);
								cell.setCellValue(rs.getString("word3"));
								cell = row.createCell(cellID++);
								cell.setCellValue(rs.getInt("freq_in_text"));
								cell = row.createCell(cellID++);
								long w1_freq = rs.getLong("word1_freq");
								cell.setCellValue(w1_freq);
								cell = row.createCell(cellID++);
								long w2_freq = rs.getLong("word2_freq");
								cell.setCellValue(w2_freq);
								cell = row.createCell(cellID++);
								long w3_freq = rs.getLong("word3_freq");
								cell.setCellValue(w3_freq);
								cell = row.createCell(cellID++);
								long b1_freq = rs.getLong("bigram1_freq");
								cell.setCellValue(b1_freq);
								cell = row.createCell(cellID++);
								long b2_freq = rs.getLong("bigram2_freq");
								cell.setCellValue(b2_freq);
								cell = row.createCell(cellID++);
								long t_freq = rs.getLong("trigram_freq");
								cell.setCellValue(t_freq);
								cell = row.createCell(cellID++);
								cell.setCellValue(rs.getDouble("in_coca"));
								cell = row.createCell(cellID++);
								cell.setCellValue(rs.getDouble("mean_freq"));
								cell = row.createCell(cellID++);
								cell.setCellValue(rs.getDouble("pmi"));
							}	
								
							r.s.executeUpdate("delete from documents_pending_lenko where document = '" + actualDocument + "';");
							r.c.commit();
			}
		
		//summative
		String summative_query = "select brl.document, "
				+ "d.round_id as ri, "
				+ "d.group_id as gi, "
				+ "wtf.wtf as word_tokens, "
				+ "sum(freq_in_text) as trigram_tokens, "
				+ "count(*) as trigram_types, "
				+ "sum(freq_in_text*in_coca) as tic_tokens, "
				+ "sum(freq_in_text*in_coca)::double precision / sum(freq_in_text) as tic_tokens_p, "
				+ "sum(in_coca) as tic_types, "
				+ "sum(in_coca)::double precision / sum(1) as tic_types_p, "
				+ "sum(freq_in_text) - sum(freq_in_text*in_coca) as tnic_tokens, "
				+ "(sum(freq_in_text) - sum(freq_in_text*in_coca))::double precision / sum(freq_in_text) as tnic_tokens_p, "
				+ "count(*) - sum(in_coca) as tnic_types, "
				+ "(count(*) - sum(in_coca))::double precision / count(*) as tnic_types_p, "
				+ "sum(freq_in_text*mean_freq*in_coca)/sum(freq_in_text*in_coca) as mean_freq_tokens, "
				+ "sum(mean_freq*in_coca)/sum(in_coca) as mean_freq_types, "
				+ "sum(pmi*freq_in_text*in_coca)/sum(freq_in_text*in_coca)::double precision as mean_pmi_tokens, "
				+ "sum(pmi*in_coca)/sum(in_coca) as mean_pmi_types "
				+ "from trigrams_raport_lenko as brl "
				+ "inner join documents_lenko as d on d.document = brl.document "
				+ "inner join word_token_frequency as wtf on wtf.d = brl.document "
				+ "group by brl.document, wtf.wtf, d.group_id, d.round_id";
		
		r.s = r.c.createStatement();
		rs = r.s.executeQuery(summative_query);
		XSSFSheet spreadsheet = workbook.createSheet("Summative");
		int rowID = 0;
		int cellID = 0;
		XSSFRow row = spreadsheet.createRow(rowID++);
		Cell cell = row.createCell(cellID++);
		cell.setCellValue("Document");
		cell = row.createCell(cellID++);
		cell.setCellValue("Round ID");
		cell = row.createCell(cellID++);
		cell.setCellValue("Group ID");
		cell = row.createCell(cellID++);
		cell.setCellValue("Word tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Trigram tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Trigram types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Trigrams in COCA tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Trigrams in COCA tokens %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Trigrams in COCA types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Trigrams in COCA types %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Trigrams not in COCA tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Trigrams not in COCA tokens %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Trigrams not in COCA types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Trigrams not in COCA types %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean frequency tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean frequency types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean PMI tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean PMI types");
		cell = row.createCell(cellID++);
		
		while(rs.next()){
			cellID = 0;
			row = spreadsheet.createRow(rowID++);
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getString("document"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("ri"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("gi"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("word_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("trigram_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("trigram_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("tic_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("tic_tokens_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("tic_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("tic_types_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("tnic_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("tnic_tokens_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("tnic_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("tnic_types_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_freq_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_freq_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_pmi_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_pmi_types"));
			cell = row.createCell(cellID++);
		}
			LocalDateTime ldt = LocalDateTime.now();
			FileOutputStream out = new FileOutputStream(new File("trigrams_" + ldt.format(DateTimeFormatter.BASIC_ISO_DATE) + ".xlsx"));
			workbook.write(out);
			workbook.close();
			out.close();
	}							
}