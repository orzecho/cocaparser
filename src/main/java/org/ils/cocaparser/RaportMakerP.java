package org.ils.cocaparser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.*;

public class RaportMakerP {
	private Connection c;
    private Statement s;
	
	public RaportMakerP(){
		this.c = null;
	    this.s = null;
	    try {
	         Class.forName("org.postgresql.Driver");
	         c = DriverManager
	            .getConnection("jdbc:postgresql://localhost:5432/nlp",
	            "orzecho", "lerchonarcom2");
	         c.setAutoCommit(false);
	      } catch (Exception e) {
	         e.printStackTrace();
	         System.err.println(e.getClass().getName()+": "+e.getMessage());
	         System.exit(0);	
	      }
	      System.out.println("Opened database successfully");
	      
	      
	}
	
	public static void main(String[] args) throws SQLException, IOException{
		generateBigramRaport();
	}

	private static void generateBigramRaport() throws SQLException, IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		RaportMakerP r = new RaportMakerP();
		
		int cellID;
			r.s = r.c.createStatement();
			ResultSet rs = r.s.executeQuery("select distinct dl.group_id as group,w1,w2,bcd.clips_post1 as t1, bcd.clips_post2 as t2, w1_freq,w2_freq,in_coca,freq_text,freq_coca,mean_freq,pmi,tscore,non_pmi_c,low_pmi_c,med_pmi_c,high_pmi_c,non_t_c, low_t_c, med_t_c, high_t_c from bigrams_raport_lenko as brl"
					+ " inner join bigram_clips_documents as bcd on bcd.word1 = brl.w1 and bcd.word2 = brl.w2 and bcd.document = brl.document"
					+ " inner join documents_lenko as dl on dl.document = brl.document;");
			XSSFSheet spreadsheet = workbook.createSheet("All with groups");
			int rowID = 0;
			cellID = 0;
			XSSFRow row = spreadsheet.createRow(rowID++);
			Cell cell = row.createCell(cellID++);
			cell.setCellValue("Group");
			cell = row.createCell(cellID++);
			cell.setCellValue("Word 1");
			cell = row.createCell(cellID++);
			cell.setCellValue("Word 2");
			cell = row.createCell(cellID++);
			cell.setCellValue("Tag 1");
			cell = row.createCell(cellID++);
			cell.setCellValue("Tag 2");
			cell = row.createCell(cellID++);
			cell.setCellValue("Word 1 frequency");
			cell = row.createCell(cellID++);
			cell.setCellValue("Word 2 frequency");
			/*cell = row.createCell(cellID++);
			cell.setCellValue("In coca?");*/
			cell = row.createCell(cellID++);
			cell.setCellValue("Frequency in text");
			cell = row.createCell(cellID++);
			cell.setCellValue("Frequency in COCA");
			cell = row.createCell(cellID++);
			cell.setCellValue("Mean frequency in COCA");
			cell = row.createCell(cellID++);
			cell.setCellValue("PMI");
			cell = row.createCell(cellID++);
			cell.setCellValue("t-score");
			/*cell = row.createCell(cellID++);
			cell.setCellValue("PMI < 3");
			cell = row.createCell(cellID++);
			cell.setCellValue("PMI in [3,5)");
			cell = row.createCell(cellID++);
			cell.setCellValue("PMI in [5,7)");
			cell = row.createCell(cellID++);
			cell.setCellValue("PMI >= 7");
			cell = row.createCell(cellID++);
			cell.setCellValue("t < 2");
			cell = row.createCell(cellID++);
			cell.setCellValue("t in [2,6)");
			cell = row.createCell(cellID++);
			cell.setCellValue("t in [6,10)");
			cell = row.createCell(cellID++);
			cell.setCellValue("t >= 10");*/
			
			while(rs.next()){
				cellID = 0;
				row = spreadsheet.createRow(rowID++);
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getString("group"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getString("w1"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getString("w2"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getString("t1"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getString("t2"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("w1_freq"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("w2_freq"));
				/*cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("in_coca"));*/
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("freq_text"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("freq_coca"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("mean_freq"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("pmi"));
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getDouble("tscore"));
				/*cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("non_pmi_c") == 1 ? "*" : "" );
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("low_pmi_c") == 1 ? "*" : "" );
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("med_pmi_c") == 1 ? "*" : "" );
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("high_pmi_c") == 1 ? "*" : "" );
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("non_t_c") == 1 ? "*" : "" );
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("low_t_c") == 1 ? "*" : "" );
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("med_t_c") == 1 ? "*" : "" );
				cell = row.createCell(cellID++);
				cell.setCellValue(rs.getInt("high_t_c") == 1 ? "*" : "" );
				 */
			
			}
			
		String summative_query = "select "
				+ "d.group_id as gi, "
				+ "sum(freq_text) as bigram_tokens, "
				+ "count(*) as bigram_types, "
				+ "sum(freq_text*in_coca) as bic_tokens, "
				+ "sum(freq_text*in_coca)::double precision / sum(freq_text) as bic_tokens_p, "
				+ "sum(in_coca) as bic_types, "
				+ "sum(in_coca)::double precision / sum(1) as bic_types_p, "
				+ "sum(freq_text) - sum(freq_text*in_coca) as bnic_tokens, "
				+ "(sum(freq_text) - sum(freq_text*in_coca))::double precision / sum(freq_text) as bnic_tokens_p, "
				+ "count(*) - sum(in_coca) as bnic_types, "
				+ "(count(*) - sum(in_coca))::double precision / count(*) as bnic_types_p, "
				+ "sum(freq_text*mean_freq*in_coca)/sum(freq_text*in_coca) as mean_freq_tokens, "
				+ "sum(mean_freq*in_coca)/sum(in_coca) as mean_freq_types, "
				+ "sum(pmi*freq_text*in_coca)/sum(freq_text*in_coca)::double precision as mean_pmi_tokens, "
				+ "sum(pmi*in_coca)/sum(in_coca) as mean_pmi_types, "
				+ "sum(tscore*freq_text*in_coca)/sum(freq_text*in_coca)::double precision as mean_t_tokens, "
				+ "sum(tscore*in_coca)/sum(in_coca) as mean_t_types, "
				+ "sum(non_pmi_c*in_coca) as non_pmi_c, "
				+ "sum(low_pmi_c*in_coca) as low_pmi_c, "
				+ "sum(med_pmi_c*in_coca) as med_pmi_c, "
				+ "sum(high_pmi_c*in_coca) as high_pmi_c, "
				+ "sum(non_t_c*in_coca) as non_t_c, "
				+ "sum(low_t_c*in_coca) as low_t_c, "
				+ "sum(med_t_c*in_coca) as med_t_c, "
				+ "sum(high_t_c*in_coca) as high_t_c, "
				+ "sum(non_pmi_c*freq_text*in_coca) as non_pmi_c_tokens, "
				+ "sum(low_pmi_c*freq_text*in_coca) as low_pmi_c_tokens, "
				+ "sum(med_pmi_c*freq_text*in_coca) as med_pmi_c_tokens, "
				+ "sum(high_pmi_c*freq_text*in_coca) as high_pmi_c_tokens, "
				+ "sum(non_t_c*freq_text*in_coca) as non_t_c_tokens, "
				+ "sum(low_t_c*freq_text*in_coca) as low_t_c_tokens, "
				+ "sum(med_t_c*freq_text*in_coca) as med_t_c_tokens, "
				+ "sum(high_t_c*freq_text*in_coca) as high_t_c_tokens, "
				+ "sum(non_pmi_c*in_coca)::double precision / sum(in_coca) as non_pmi_c_p, "
				+ "sum(low_pmi_c*in_coca)::double precision / sum(in_coca) as low_pmi_c_p, "
				+ "sum(med_pmi_c*in_coca)::double precision / sum(in_coca) as med_pmi_c_p, "
				+ "sum(high_pmi_c*in_coca)::double precision / sum(in_coca) as high_pmi_c_p, "
				+ "sum(non_t_c*in_coca)::double precision / sum(in_coca) as non_t_c_p, "
				+ "sum(low_t_c*in_coca)::double precision / sum(in_coca) as low_t_c_p, "
				+ "sum(med_t_c*in_coca)::double precision / sum(in_coca) as med_t_c_p, "
				+ "sum(high_t_c*in_coca)::double precision / sum(in_coca) as high_t_c_p, "
				+ "sum(non_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as non_pmi_c_p_tokens, "
				+ "sum(low_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as low_pmi_c_p_tokens, "
				+ "sum(med_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as med_pmi_c_p_tokens, "
				+ "sum(high_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as high_pmi_c_p_tokens, "
				+ "sum(non_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as non_t_c_p_tokens, "
				+ "sum(low_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as low_t_c_p_tokens, "
				+ "sum(med_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as med_t_c_p_tokens, "
				+ "sum(high_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as high_t_c_p_tokens "
				+ "from bigrams_raport_lenko as brl inner join documents_lenko as d on d.document = brl.document "
				+ "group by d.group_id";
		
		r.s = r.c.createStatement();
		rs = r.s.executeQuery(summative_query);

		spreadsheet = workbook.createSheet("Summative");
		rowID = 0;
		cellID = 0;
		row = spreadsheet.createRow(rowID++);
		cell = row.createCell(cellID++);
		cell.setCellValue("Group ID");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigram tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigram types");
		/*cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams in COCA tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams in COCA tokens %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams in COCA types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams in COCA types %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams not in COCA tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams not in COCA tokens %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams not in COCA types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams not in COCA types %");
		cell = row.createCell(cellID++);*/
		cell.setCellValue("Mean frequency tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean frequency types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean PMI tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean PMI types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean t-score tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean t-score types");
		/*cell = row.createCell(cellID++);
		cell.setCellValue("Non PMI collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low PMI collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium PMI collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("High PMI collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non t-score collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low t-score collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium t-score collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("High t-score collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non PMI collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low PMI collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium PMI collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("High PMI collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non t-score collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low t-score collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium t-score collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("High t-score collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non PMI collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low PMI collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium PMI collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("High PMI collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non t-score collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low t-score collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium t-score collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("High t-score collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non PMI collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low PMI collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium PMI collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("High PMI collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non t-score collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low t-score collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium t-score collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("High t-score collocations % tokens");*/
		
		while(rs.next()){
			cellID = 0;
			row = spreadsheet.createRow(rowID++);
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("gi"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bigram_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bigram_types"));
			/*cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bic_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("bic_tokens_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bic_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("bic_types_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bnic_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("bnic_tokens_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bnic_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("bnic_types_p"));
			cell = row.createCell(cellID++);*/
			cell.setCellValue(rs.getDouble("mean_freq_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_freq_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_pmi_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_pmi_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_t_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_t_types"));
			/*cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("non_pmi_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("low_pmi_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("med_pmi_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("high_pmi_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("non_t_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("low_t_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("med_t_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("high_t_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("non_pmi_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("low_pmi_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("med_pmi_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("high_pmi_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("non_t_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("low_t_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("med_t_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("high_t_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("non_pmi_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("low_pmi_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("med_pmi_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("high_pmi_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("non_t_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("low_t_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("med_t_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("high_t_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("non_pmi_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("low_pmi_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("med_pmi_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("high_pmi_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("non_t_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("low_t_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("med_t_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("high_t_c_p_tokens"));*/
		}
		
		String summative_query_NN_NN = "select "
				+ "d.group_id as gi, "
				+ "sum(freq_text) as bigram_tokens, "
				+ "count(*) as bigram_types, "
				+ "sum(freq_text*in_coca) as bic_tokens, "
				+ "sum(freq_text*in_coca)::double precision / sum(freq_text) as bic_tokens_p, "
				+ "sum(in_coca) as bic_types, "
				+ "sum(in_coca)::double precision / sum(1) as bic_types_p, "
				+ "sum(freq_text) - sum(freq_text*in_coca) as bnic_tokens, "
				+ "(sum(freq_text) - sum(freq_text*in_coca))::double precision / sum(freq_text) as bnic_tokens_p, "
				+ "count(*) - sum(in_coca) as bnic_types, "
				+ "(count(*) - sum(in_coca))::double precision / count(*) as bnic_types_p, "
				+ "sum(freq_text*mean_freq*in_coca)/sum(freq_text*in_coca) as mean_freq_tokens, "
				+ "sum(mean_freq*in_coca)/sum(in_coca) as mean_freq_types, "
				+ "sum(pmi*freq_text*in_coca)/sum(freq_text*in_coca)::double precision as mean_pmi_tokens, "
				+ "sum(pmi*in_coca)/sum(in_coca) as mean_pmi_types, "
				+ "sum(tscore*freq_text*in_coca)/sum(freq_text*in_coca)::double precision as mean_t_tokens, "
				+ "sum(tscore*in_coca)/sum(in_coca) as mean_t_types, "
				+ "sum(non_pmi_c*in_coca) as non_pmi_c, "
				+ "sum(low_pmi_c*in_coca) as low_pmi_c, "
				+ "sum(med_pmi_c*in_coca) as med_pmi_c, "
				+ "sum(high_pmi_c*in_coca) as high_pmi_c, "
				+ "sum(non_t_c*in_coca) as non_t_c, "
				+ "sum(low_t_c*in_coca) as low_t_c, "
				+ "sum(med_t_c*in_coca) as med_t_c, "
				+ "sum(high_t_c*in_coca) as high_t_c, "
				+ "sum(non_pmi_c*freq_text*in_coca) as non_pmi_c_tokens, "
				+ "sum(low_pmi_c*freq_text*in_coca) as low_pmi_c_tokens, "
				+ "sum(med_pmi_c*freq_text*in_coca) as med_pmi_c_tokens, "
				+ "sum(high_pmi_c*freq_text*in_coca) as high_pmi_c_tokens, "
				+ "sum(non_t_c*freq_text*in_coca) as non_t_c_tokens, "
				+ "sum(low_t_c*freq_text*in_coca) as low_t_c_tokens, "
				+ "sum(med_t_c*freq_text*in_coca) as med_t_c_tokens, "
				+ "sum(high_t_c*freq_text*in_coca) as high_t_c_tokens, "
				+ "sum(non_pmi_c*in_coca)::double precision / sum(in_coca) as non_pmi_c_p, "
				+ "sum(low_pmi_c*in_coca)::double precision / sum(in_coca) as low_pmi_c_p, "
				+ "sum(med_pmi_c*in_coca)::double precision / sum(in_coca) as med_pmi_c_p, "
				+ "sum(high_pmi_c*in_coca)::double precision / sum(in_coca) as high_pmi_c_p, "
				+ "sum(non_t_c*in_coca)::double precision / sum(in_coca) as non_t_c_p, "
				+ "sum(low_t_c*in_coca)::double precision / sum(in_coca) as low_t_c_p, "
				+ "sum(med_t_c*in_coca)::double precision / sum(in_coca) as med_t_c_p, "
				+ "sum(high_t_c*in_coca)::double precision / sum(in_coca) as high_t_c_p, "
				+ "sum(non_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as non_pmi_c_p_tokens, "
				+ "sum(low_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as low_pmi_c_p_tokens, "
				+ "sum(med_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as med_pmi_c_p_tokens, "
				+ "sum(high_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as high_pmi_c_p_tokens, "
				+ "sum(non_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as non_t_c_p_tokens, "
				+ "sum(low_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as low_t_c_p_tokens, "
				+ "sum(med_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as med_t_c_p_tokens, "
				+ "sum(high_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as high_t_c_p_tokens "
				+ "from bigrams_raport_lenko as brl inner join documents_lenko as d on d.document = brl.document "
				+ "inner join bigram_clips_documents as bcd on bcd.word1 = brl.w1 and bcd.word2 = brl.w2 and bcd.document = brl.document "
				+ "where bcd.clips_post1 like 'NN%' and bcd.clips_post2 like 'NN%' "
				+ "group by d.group_id";
		
		r.s = r.c.createStatement();
		rs = r.s.executeQuery(summative_query_NN_NN);
		XSSFSheet spreadsheet_NN_NN = workbook.createSheet("Summative NN NN");
		rowID = 0;
		cellID = 0;
		row = spreadsheet_NN_NN.createRow(rowID++);
		cell = row.createCell(cellID++);
		cell.setCellValue("Group ID");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigram tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigram types");
		/*cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams in COCA tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams in COCA tokens %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams in COCA types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams in COCA types %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams not in COCA tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams not in COCA tokens %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams not in COCA types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams not in COCA types %");
		cell = row.createCell(cellID++);*/
		cell.setCellValue("Mean frequency tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean frequency types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean PMI tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean PMI types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean t-score tokens");
		/*cell = row.createCell(cellID++);
		cell.setCellValue("Mean t-score types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non PMI collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low PMI collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium PMI collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("High PMI collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non t-score collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low t-score collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium t-score collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("High t-score collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non PMI collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low PMI collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium PMI collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("High PMI collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non t-score collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low t-score collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium t-score collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("High t-score collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non PMI collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low PMI collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium PMI collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("High PMI collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non t-score collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low t-score collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium t-score collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("High t-score collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non PMI collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low PMI collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium PMI collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("High PMI collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non t-score collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low t-score collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium t-score collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("High t-score collocations % tokens");
		*/
		while(rs.next()){
			cellID = 0;
			row = spreadsheet_NN_NN.createRow(rowID++);
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("gi"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bigram_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bigram_types"));
			cell = row.createCell(cellID++);
			/*cell.setCellValue(rs.getInt("bic_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("bic_tokens_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bic_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("bic_types_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bnic_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("bnic_tokens_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bnic_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("bnic_types_p"));
			cell = row.createCell(cellID++);*/
			cell.setCellValue(rs.getDouble("mean_freq_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_freq_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_pmi_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_pmi_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_t_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_t_types"));
			/*cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("non_pmi_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("low_pmi_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("med_pmi_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("high_pmi_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("non_t_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("low_t_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("med_t_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("high_t_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("non_pmi_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("low_pmi_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("med_pmi_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("high_pmi_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("non_t_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("low_t_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("med_t_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("high_t_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("non_pmi_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("low_pmi_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("med_pmi_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("high_pmi_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("non_t_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("low_t_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("med_t_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("high_t_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("non_pmi_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("low_pmi_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("med_pmi_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("high_pmi_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("non_t_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("low_t_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("med_t_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("high_t_c_p_tokens"));*/
		}
		
		String summative_query_JJ_NN = "select "
				+ "d.group_id as gi, "
				+ "sum(freq_text) as bigram_tokens, "
				+ "count(*) as bigram_types, "
				+ "sum(freq_text*in_coca) as bic_tokens, "
				+ "sum(freq_text*in_coca)::double precision / sum(freq_text) as bic_tokens_p, "
				+ "sum(in_coca) as bic_types, "
				+ "sum(in_coca)::double precision / sum(1) as bic_types_p, "
				+ "sum(freq_text) - sum(freq_text*in_coca) as bnic_tokens, "
				+ "(sum(freq_text) - sum(freq_text*in_coca))::double precision / sum(freq_text) as bnic_tokens_p, "
				+ "count(*) - sum(in_coca) as bnic_types, "
				+ "(count(*) - sum(in_coca))::double precision / count(*) as bnic_types_p, "
				+ "sum(freq_text*mean_freq*in_coca)/sum(freq_text*in_coca) as mean_freq_tokens, "
				+ "sum(mean_freq*in_coca)/sum(in_coca) as mean_freq_types, "
				+ "sum(pmi*freq_text*in_coca)/sum(freq_text*in_coca)::double precision as mean_pmi_tokens, "
				+ "sum(pmi*in_coca)/sum(in_coca) as mean_pmi_types, "
				+ "sum(tscore*freq_text*in_coca)/sum(freq_text*in_coca)::double precision as mean_t_tokens, "
				+ "sum(tscore*in_coca)/sum(in_coca) as mean_t_types, "
				+ "sum(non_pmi_c*in_coca) as non_pmi_c, "
				+ "sum(low_pmi_c*in_coca) as low_pmi_c, "
				+ "sum(med_pmi_c*in_coca) as med_pmi_c, "
				+ "sum(high_pmi_c*in_coca) as high_pmi_c, "
				+ "sum(non_t_c*in_coca) as non_t_c, "
				+ "sum(low_t_c*in_coca) as low_t_c, "
				+ "sum(med_t_c*in_coca) as med_t_c, "
				+ "sum(high_t_c*in_coca) as high_t_c, "
				+ "sum(non_pmi_c*freq_text*in_coca) as non_pmi_c_tokens, "
				+ "sum(low_pmi_c*freq_text*in_coca) as low_pmi_c_tokens, "
				+ "sum(med_pmi_c*freq_text*in_coca) as med_pmi_c_tokens, "
				+ "sum(high_pmi_c*freq_text*in_coca) as high_pmi_c_tokens, "
				+ "sum(non_t_c*freq_text*in_coca) as non_t_c_tokens, "
				+ "sum(low_t_c*freq_text*in_coca) as low_t_c_tokens, "
				+ "sum(med_t_c*freq_text*in_coca) as med_t_c_tokens, "
				+ "sum(high_t_c*freq_text*in_coca) as high_t_c_tokens, "
				+ "sum(non_pmi_c*in_coca)::double precision / sum(in_coca) as non_pmi_c_p, "
				+ "sum(low_pmi_c*in_coca)::double precision / sum(in_coca) as low_pmi_c_p, "
				+ "sum(med_pmi_c*in_coca)::double precision / sum(in_coca) as med_pmi_c_p, "
				+ "sum(high_pmi_c*in_coca)::double precision / sum(in_coca) as high_pmi_c_p, "
				+ "sum(non_t_c*in_coca)::double precision / sum(in_coca) as non_t_c_p, "
				+ "sum(low_t_c*in_coca)::double precision / sum(in_coca) as low_t_c_p, "
				+ "sum(med_t_c*in_coca)::double precision / sum(in_coca) as med_t_c_p, "
				+ "sum(high_t_c*in_coca)::double precision / sum(in_coca) as high_t_c_p, "
				+ "sum(non_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as non_pmi_c_p_tokens, "
				+ "sum(low_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as low_pmi_c_p_tokens, "
				+ "sum(med_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as med_pmi_c_p_tokens, "
				+ "sum(high_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as high_pmi_c_p_tokens, "
				+ "sum(non_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as non_t_c_p_tokens, "
				+ "sum(low_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as low_t_c_p_tokens, "
				+ "sum(med_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as med_t_c_p_tokens, "
				+ "sum(high_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as high_t_c_p_tokens "
				+ "from bigrams_raport_lenko as brl inner join documents_lenko as d on d.document = brl.document "
				+ "inner join bigram_clips_documents as bcd on bcd.word1 = brl.w1 and bcd.word2 = brl.w2 and bcd.document = brl.document "
				+ "where bcd.clips_post1 like 'JJ%' and bcd.clips_post2 like 'NN%' "
				+ "group by d.group_id";
		
		r.s = r.c.createStatement();
		rs = r.s.executeQuery(summative_query_JJ_NN);
		XSSFSheet spreadsheet_JJ_NN = workbook.createSheet("Summative JJ NN");
		rowID = 0;
		cellID = 0;
		row = spreadsheet_JJ_NN.createRow(rowID++);
		cell = row.createCell(cellID++);
		cell.setCellValue("Group ID");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigram tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigram types");
		/*cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams in COCA tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams in COCA tokens %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams in COCA types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams in COCA types %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams not in COCA tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams not in COCA tokens %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams not in COCA types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Bigrams not in COCA types %");
		cell = row.createCell(cellID++);*/
		cell.setCellValue("Mean frequency tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean frequency types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean PMI tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean PMI types");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean t-score tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Mean t-score types");
		/*cell = row.createCell(cellID++);
		cell.setCellValue("Non PMI collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low PMI collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium PMI collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("High PMI collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non t-score collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low t-score collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium t-score collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("High t-score collocations");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non PMI collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low PMI collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium PMI collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("High PMI collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non t-score collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low t-score collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium t-score collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("High t-score collocations %");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non PMI collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low PMI collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium PMI collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("High PMI collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non t-score collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low t-score collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium t-score collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("High t-score collocations tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non PMI collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low PMI collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium PMI collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("High PMI collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Non t-score collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Low t-score collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("Medium t-score collocations % tokens");
		cell = row.createCell(cellID++);
		cell.setCellValue("High t-score collocations % tokens");
		*/
		while(rs.next()){
			cellID = 0;
			row = spreadsheet_JJ_NN.createRow(rowID++);
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("gi"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bigram_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bigram_types"));
			/*cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bic_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("bic_tokens_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bic_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("bic_types_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bnic_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("bnic_tokens_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("bnic_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("bnic_types_p"));*/
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_freq_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_freq_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_pmi_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_pmi_types"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_t_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("mean_t_types"));
			/*cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("non_pmi_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("low_pmi_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("med_pmi_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("high_pmi_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("non_t_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("low_t_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("med_t_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("high_t_c"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("non_pmi_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("low_pmi_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("med_pmi_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("high_pmi_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("non_t_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("low_t_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("med_t_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("high_t_c_p"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("non_pmi_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("low_pmi_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("med_pmi_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("high_pmi_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("non_t_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("low_t_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("med_t_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getInt("high_t_c_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("non_pmi_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("low_pmi_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("med_pmi_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("high_pmi_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("non_t_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("low_t_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("med_t_c_p_tokens"));
			cell = row.createCell(cellID++);
			cell.setCellValue(rs.getDouble("high_t_c_p_tokens"));*/
		}
		
			LocalDateTime ldt = LocalDateTime.now();
			FileOutputStream out = new FileOutputStream(new File("jola_raport_" + ldt.format(DateTimeFormatter.BASIC_ISO_DATE) + ".xlsx"));
			workbook.write(out);
			out.close();
		
	}							
}