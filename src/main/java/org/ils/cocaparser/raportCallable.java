package org.ils.cocaparser;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.ReentrantLock;

public class raportCallable implements Callable<Integer>{
	String actualDocument;
	Connection c;
	List<String> documents;
	Integer ticket;
	
	public raportCallable(Connection c, String actualDocument, Integer ticket) {
		super();
		this.c = c;
		this.actualDocument = actualDocument;
		
		this.ticket = ticket;
		
	}
	
	@Override
	public Integer call() throws Exception {
			System.out.println("Making raport from document: " + actualDocument + " - ticket: " + this.ticket);
			Statement s = c.createStatement();
			s.executeUpdate("insert into bigrams_raport_lenko (document,w1,w2,freq_text,in_coca,freq_coca,w1_freq,w2_freq,mean_freq,pmi,tscore) select "
			+ "b.document, "
			+ "b.w1, b.w2, "
			+ "b.freq as text_freq, "
			+ "case when c.sum is null then 0 else 1 end, "
			+ "c.sum as coca_freq, "
			+ "u1.freq, u2.freq, "
			+ "c.sum::double precision/425::double precision as mean, "
			+ "log(2.0::numeric,(c.sum::double precision/((u1.freq::double precision*u2.freq::double precision)/425000000::double precision))::numeric) as pmi, "
			+ "(c.sum::double precision - ((u1.freq::double precision*u2.freq::double precision)/425000000::double precision))/sqrt(c.sum::double precision) as tscore "
			+ "from bigrams_types_lenko as b "
			+ "left join unigrams_lenko as u1 on u1.w1 = b.w1 left join unigrams_lenko as u2 on u2.w1 = b.w2 left join cocabigrams_lenko as c on c.w1 = b.w1 and c.w2 = b.w2 where b.document = '" + documents.get(0) + "';");
			s.executeUpdate("delete from documents_pending_lenko where document = '" + actualDocument + "';");
			c.commit();
			s.close();
			System.out.println("Done with " + actualDocument + " - ticket: " + this.ticket);
			return 1;
	}


}
