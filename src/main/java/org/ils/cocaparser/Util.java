package org.ils.cocaparser;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import opennlp.tools.tokenize.SimpleTokenizer;
import opennlp.tools.tokenize.Tokenizer;

public class Util {
	public static void main(String args[]){
		
		System.out.println("update bigrams_raport_lenko set low_pmi_c = case when pmi>=3 and pmi<5 then 1 else 0 end; "
				+ "update bigrams_raport_lenko set med_pmi_c = case when pmi>=5 and pmi<7 then 1 else 0 end; "
				+ "update bigrams_raport_lenko set high_pmi_c = case when pmi>=7 then 1 else 0 end; "
				+ "update bigrams_raport_lenko set non_t_c = case when tscore<2 then 1 else 0 end; "
				+ "update bigrams_raport_lenko set low_t_c = case when tscore>=2 and tscore<6 then 1 else 0 end; "
				+ "update bigrams_raport_lenko set med_t_c = case when tscore>=6 and tscore<10 then 1 else 0 end; "
				+ "update bigrams_raport_lenko set high_t_c = case when tscore>=10 then 1 else 0 end;");
		
		String summative_query_NN_NN = "select "
				+ "d.group_id as gi, "
				+ "sum(freq_text) as bigram_tokens, "
				+ "count(*) as bigram_types, "
				+ "sum(freq_text*in_coca) as bic_tokens, "
				+ "sum(freq_text*in_coca)::double precision / sum(freq_text) as bic_tokens_p, "
				+ "sum(in_coca) as bic_types, "
				+ "sum(in_coca)::double precision / sum(1) as bic_types_p, "
				+ "sum(freq_text) - sum(freq_text*in_coca) as bnic_tokens, "
				+ "(sum(freq_text) - sum(freq_text*in_coca))::double precision / sum(freq_text) as bnic_tokens_p, "
				+ "count(*) - sum(in_coca) as bnic_types, "
				+ "(count(*) - sum(in_coca))::double precision / count(*) as bnic_types_p, "
				+ "sum(freq_text*mean_freq*in_coca)/sum(freq_text*in_coca) as mean_freq_tokens, "
				+ "sum(mean_freq*in_coca)/sum(in_coca) as mean_freq_types, "
				+ "sum(pmi*freq_text*in_coca)/sum(freq_text*in_coca)::double precision as mean_pmi_tokens, "
				+ "sum(pmi*in_coca)/sum(in_coca) as mean_pmi_types, "
				+ "sum(tscore*freq_text*in_coca)/sum(freq_text*in_coca)::double precision as mean_t_tokens, "
				+ "sum(tscore*in_coca)/sum(in_coca) as mean_t_types, "
				+ "sum(non_pmi_c*in_coca) as non_pmi_c, "
				+ "sum(low_pmi_c*in_coca) as low_pmi_c, "
				+ "sum(med_pmi_c*in_coca) as med_pmi_c, "
				+ "sum(high_pmi_c*in_coca) as high_pmi_c, "
				+ "sum(non_t_c*in_coca) as non_t_c, "
				+ "sum(low_t_c*in_coca) as low_t_c, "
				+ "sum(med_t_c*in_coca) as med_t_c, "
				+ "sum(high_t_c*in_coca) as high_t_c, "
				+ "sum(non_pmi_c*freq_text*in_coca) as non_pmi_c_tokens, "
				+ "sum(low_pmi_c*freq_text*in_coca) as low_pmi_c_tokens, "
				+ "sum(med_pmi_c*freq_text*in_coca) as med_pmi_c_tokens, "
				+ "sum(high_pmi_c*freq_text*in_coca) as high_pmi_c_tokens, "
				+ "sum(non_t_c*freq_text*in_coca) as non_t_c_tokens, "
				+ "sum(low_t_c*freq_text*in_coca) as low_t_c_tokens, "
				+ "sum(med_t_c*freq_text*in_coca) as med_t_c_tokens, "
				+ "sum(high_t_c*freq_text*in_coca) as high_t_c_tokens, "
				+ "sum(non_pmi_c*in_coca)::double precision / sum(in_coca) as non_pmi_c_p, "
				+ "sum(low_pmi_c*in_coca)::double precision / sum(in_coca) as low_pmi_c_p, "
				+ "sum(med_pmi_c*in_coca)::double precision / sum(in_coca) as med_pmi_c_p, "
				+ "sum(high_pmi_c*in_coca)::double precision / sum(in_coca) as high_pmi_c_p, "
				+ "sum(non_t_c*in_coca)::double precision / sum(in_coca) as non_t_c_p, "
				+ "sum(low_t_c*in_coca)::double precision / sum(in_coca) as low_t_c_p, "
				+ "sum(med_t_c*in_coca)::double precision / sum(in_coca) as med_t_c_p, "
				+ "sum(high_t_c*in_coca)::double precision / sum(in_coca) as high_t_c_p, "
				+ "sum(non_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as non_pmi_c_p_tokens, "
				+ "sum(low_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as low_pmi_c_p_tokens, "
				+ "sum(med_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as med_pmi_c_p_tokens, "
				+ "sum(high_pmi_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as high_pmi_c_p_tokens, "
				+ "sum(non_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as non_t_c_p_tokens, "
				+ "sum(low_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as low_t_c_p_tokens, "
				+ "sum(med_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as med_t_c_p_tokens, "
				+ "sum(high_t_c*freq_text*in_coca)::double precision / sum(in_coca*freq_text) as high_t_c_p_tokens "
				+ "from bigrams_raport_lenko as brl inner join documents_lenko as d on d.document = brl.document "
				+ "inner join bigram_clips_documents as bcd on bcd.word1 = brl.w1 and bcd.word2 = brl.w2 and bcd.document = brl.document "
				+ "where bcd.clips_post1 like 'NN%' and bcd.clips_post2 like 'NN%' "
				+ "group by d.group_id";
		//	System.out.println(summative_query_NN_NN);
	
	}
}
