package org.ils.cocaparser;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;

import opennlp.tools.tokenize.SimpleTokenizer;
import opennlp.tools.tokenize.Tokenizer;
/**
 * Hello world!
 *
 */
public class Parser 
{
	private static List<String> badTokens;
	private String documentContent;
	private String tokens[];
	//private TokenizerModel tModel;
	private Connection c;
    private Statement s;
	
	public Parser(){
		/*InputStream modelIn = null;
		try {
			modelIn = new FileInputStream("en-token.bin");
			this.tModel = new TokenizerModel(modelIn);
		}
		catch (IOException e) {
		  e.printStackTrace();
		}
		finally {
		  if (modelIn != null) {
		    try {
		      modelIn.close();
		    }
		    catch (IOException e) {
		    }
		  }
		}*/
		this.c = null;
	    this.s = null;
	    try {
	         Class.forName("org.postgresql.Driver");
	         c = DriverManager
	            .getConnection("jdbc:postgresql://localhost:5432/nlp",
	            "orzecho", "lerchonarcom2");
	         c.setAutoCommit(false);
	      } catch (Exception e) {
	         e.printStackTrace();
	         System.err.println(e.getClass().getName()+": "+e.getMessage());
	         System.exit(0);	
	      }
	      System.out.println("Opened database successfully");
		
	}
	
	public Parser loadFile(String f){
		try {
			this.documentContent = readFile(f,StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public Parser tokenize(){
		//Tokenizer tokenizer = new TokenizerME(this.tModel);
		Tokenizer tokenizer = SimpleTokenizer.INSTANCE;
		this.tokens = tokenizer.tokenize(this.documentContent);
		for(int i = 0;i<this.tokens.length;i++){
			this.tokens[i] = this.tokens[i].toLowerCase();
		}
		return this;
	}
	
    public static void main( String[] args )
    {
        System.out.println("Creating Parser object.");
        Parser p = new Parser();
    	boolean proc = args[0].equals("proc") ? true : false;
    	boolean raport = args[0].equals("raport") ? true : false;
    	boolean all = args[0].equals("all") ? true : false;
    	boolean createPendingDocuments = 	args[1].equals("cpd") ? true : false;
    	if(args[2].equals("clean")){
    		try {
				p.s = p.c.createStatement();
				p.s.executeUpdate("truncate unigrams_tokens_raw_lenko;");
				p.s.executeUpdate("truncate unigrams_tokens_clean_lenko;");
				p.s.executeUpdate("truncate bigrams_tokens_raw_lenko;");
				p.s.executeUpdate("truncate bigrams_tokens_clean_lenko;");
				p.s.executeUpdate("truncate trigrams_tokens_raw_lenko;");
				p.s.executeUpdate("truncate trigrams_tokens_clean_lenko;");
				p.s.executeUpdate("truncate unigrams_types_lenko;");
				p.s.executeUpdate("truncate bigrams_types_lenko;");
				p.s.executeUpdate("truncate trigrams_types_lenko;");
				p.s.executeUpdate("truncate bigrams_raport_lenko;");
				p.s.executeUpdate("truncate documents_pending_lenko;");
				p.s.execute("select setval('unigrams_types_lenko_id_seq',1);");
				p.s.execute("select setval('bigrams_types_lenko_id_seq',1);");
				p.s.execute("select setval('trigrams_types_lenko_id_seq',1);");
				p.s.execute("select setval('bigrams_raport_lenko_id_seq',1);");
			    p.c.commit();
				p.s.close();
	    	
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
    	
    	
    	if(proc || all){
    	badTokens = new ArrayList<>();
		badTokens.add("");
		badTokens.add(" ");
		badTokens.add(".");
		badTokens.add("..");
		badTokens.add("...");
		badTokens.add(":");
		badTokens.add(";");
		badTokens.add(",");
		badTokens.add(",,");
		badTokens.add("-");
		badTokens.add("?");
		badTokens.add("!");
		badTokens.add("!!");
		badTokens.add("!!!");
		badTokens.add("???");
		badTokens.add(")");
		badTokens.add("(");
		badTokens.add("\"");
		badTokens.add("/");
		badTokens.add("–");
        
        try {
			Files.walk(Paths.get("essays_utf8/")).forEach(f -> {
				if(Files.isRegularFile(f)){
					process(p, new Document(f.toString()));
				}
				});
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
        
        aggregate(p);
        
        
        }
    	if(raport || all){
        	
    		if(createPendingDocuments){
    			try{
    				p.s = p.c.createStatement();
    				p.s.executeUpdate("truncate documents_pending_lenko;");
    				p.s.executeUpdate("insert into documents_pending_lenko select document from bigrams_types_lenko group by document;");
    			    p.c.commit();
    				p.s.close();
    			}
    			catch (SQLException e) {
    				e.printStackTrace();
    			}
    		}
    		List<String> documents = new ArrayList<>();
    		try {
    			p.s = p.c.createStatement();
				ResultSet rs = p.s.executeQuery("select document from documents_pending_lenko;");
				while(rs.next()){
					documents.add(rs.getString("document"));
				}
				List<Callable<Integer>> threadList = new ArrayList<>();
				Integer ticket = 0;
				for(int i = 0; i < documents.size(); i++){
					String actualDocument = documents.get(i);
					threadList.add(new raportCallable(p.c,actualDocument,ticket++));
				}
				ExecutorService fourwayExecutor = Executors.newFixedThreadPool(4);
				try {
					List<Future<Integer>> futures = fourwayExecutor.invokeAll(threadList);
					//fourwayExecutor.invokeAny(threadList);
					fourwayExecutor.shutdown();
					fourwayExecutor.awaitTermination(3,TimeUnit.DAYS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.print("Categorizing... ");
				p.s.executeUpdate(""
						+ "update bigrams_raport_lenko set non_pmi_c = case when pmi<3 then 1 else 0 end; "
						+ "update bigrams_raport_lenko set low_pmi_c = case when pmi>=3 and pmi<5 then 1 else 0 end; "
						+ "update bigrams_raport_lenko set med_pmi_c = case when pmi>=5 and pmi<7 then 1 else 0 end; "
						+ "update bigrams_raport_lenko set high_pmi_c = case when pmi>=7 then 1 else 0 end; "
						+ "update bigrams_raport_lenko set non_t_c = case when tscore<2 then 1 else 0 end; "
						+ "update bigrams_raport_lenko set low_t_c = case when tscore>=2 and tscore<6 then 1 else 0 end; "
						+ "update bigrams_raport_lenko set med_t_c = case when tscore>=6 and tscore<10 then 1 else 0 end; "
						+ "update bigrams_raport_lenko set high_t_c = case when tscore>=10 then 1 else 0 end;");
				p.c.commit();
				System.out.println("Done.");
				String summative_query = "select document, "
						+ "null as word_token, "
						+ "sum(freq_text) as bigram_tokens, "
						+ "count(*) as bigram_types, "
						+ "sum(freq_text*in_coca) as bic_tokens, "
						+ "sum(freq_text*in_coca)::double precision / sum(freq_text) as bic_tokens_p, "
						+ "sum(in_coca) as bic_types, "
						+ "sum(in_coca)::double precision / sum(1) as bic_types_p, "
						+ "sum(freq_text) - sum(freq_text*in_coca) as bnic_tokens, "
						+ "(sum(freq_text) - sum(freq_text*in_coca))::double precision / sum(freq_text) as bnic_tokens_p, "
						+ "count(*) - sum(in_coca) as bnic_types, "
						+ "(count(*) - sum(in_coca))::double precision / count(*) as bnic_types_p, "
						+ "sum(freq_text*mean_freq*in_coca)/sum(freq_text*in_coca) as mean_freq_tokens, "
						+ "sum(mean_freq*in_coca)/sum(in_coca) as mean_freq_types, "
						+ "sum(pmi*freq_text*in_coca)/sum(freq_text*in_coca)::double precision as mean_pmi_tokens, "
						+ "sum(pmi*in_coca)/sum(in_coca) as mean_pmi_types, "
						+ "sum(tscore*freq_text*in_coca)/sum(freq_text*in_coca)::double precision as mean_t_tokens, "
						+ "sum(tscore*in_coca)/sum(in_coca) as mean_t_types, "
						+ "sum(non_pmi_c) as non_pmi_c, "
						+ "sum(low_pmi_c) as low_pmi_c, "
						+ "sum(med_pmi_c) as med_pmi_c, "
						+ "sum(high_pmi_c) as high_pmi_c, "
						+ "sum(non_t_c) as non_t_c, "
						+ "sum(low_t_c) as low_t_c, "
						+ "sum(med_t_c) as med_t_c, "
						+ "sum(high_t_c) as high_t_c, "
						+ "sum(non_pmi_c)::double precision / sum(in_coca) as non_pmi_c_p, "
						+ "sum(low_pmi_c)::double precision / sum(in_coca) as low_pmi_c_p, "
						+ "sum(med_pmi_c)::double precision / sum(in_coca) as med_pmi_c_p, "
						+ "sum(high_pmi_c)::double precision / sum(in_coca) as high_pmi_c_p, "
						+ "sum(non_t_c)::double precision / sum(in_coca) as non_t_c_p, "
						+ "sum(low_t_c)::double precision / sum(in_coca) as low_t_c_p, "
						+ "sum(med_t_c)::double precision / sum(in_coca) as med_t_c_p, "
						+ "sum(high_t_c)::double precision / sum(in_coca) as high_t_c_p "
						+ "from bigrams_raport_lenko group by document";
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        
    	try {
        	System.out.println("Closing connection now.");
			p.c.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
    	
    }

	private static void process(Parser p, Document doc) {
        //tokenize
		System.out.println("Loading file: " + doc.filePath + " : " + doc.fileName + " and tokenizing.");
		p.loadFile(doc.filePath);
		p.documentContent = p.documentContent.replace("-", "xqx");
		p.documentContent = p.documentContent.replace("’", "'");
		p.documentContent = p.documentContent.replace("“", "\"");
		p.documentContent = p.documentContent.replace("”", "\"");
		p.tokenize();
		
		//troublesome tokenizing of "-" workaround
		for(int i = 0;i<p.tokens.length;i++){
			p.tokens[i] = p.tokens[i].replace("xqx", "-");
		}
		
		for(int i = 0;i<p.tokens.length-1;i++){
			if(p.tokens[i].equals("'") && p.tokens[i+1].equals("s")){
				p.tokens[i] = "'s";
				p.tokens = ArrayUtils.remove(p.tokens, i+1);
			}
			else if(p.tokens[i].equals("mr") && p.tokens[i+1].equals(".")){
				p.tokens[i] = "mr";
				p.tokens = ArrayUtils.remove(p.tokens, i+1);
			}
			else if(p.tokens[i].equals("mrs") && p.tokens[i+1].equals(".")){
				p.tokens[i] = "mrs";
				p.tokens = ArrayUtils.remove(p.tokens, i+1);
			}
			else if(p.tokens[i].equals("ms") && p.tokens[i+1].equals(".")){
				p.tokens[i] = "ms";
				p.tokens = ArrayUtils.remove(p.tokens, i+1);
			}
			else if(i+2 < p.tokens.length && p.tokens[i].equals("o") && p.tokens[i+1].equals("'") && p.tokens[i+2].equals("clock")){
				p.tokens[i] = "o'clock";
				p.tokens = ArrayUtils.remove(p.tokens, i+1);
				p.tokens = ArrayUtils.remove(p.tokens, i+1);
			}
			else if(i+3 < p.tokens.length && p.tokens[i].equals("e") && p.tokens[i+1].equals(".") && p.tokens[i+2].equals("g") && p.tokens[i+3].equals(".")){
				p.tokens[i] = ".";
				p.tokens = ArrayUtils.remove(p.tokens, i+1);
				p.tokens = ArrayUtils.remove(p.tokens, i+1);
				p.tokens = ArrayUtils.remove(p.tokens, i+1);
			}
			else if(i+3 < p.tokens.length && p.tokens[i].equals("i") && p.tokens[i+1].equals(".") && p.tokens[i+2].equals("e") && p.tokens[i+3].equals(".")){
				p.tokens[i] = ".";
				p.tokens = ArrayUtils.remove(p.tokens, i+1);
				p.tokens = ArrayUtils.remove(p.tokens, i+1);
				p.tokens = ArrayUtils.remove(p.tokens, i+1);
			}
		}
		System.out.println("Tokenized.");
		p.tokens = ArrayUtils.remove(p.tokens,0);
	    //get unigrams
		System.out.print("Getting unigrams... ");
	    for(String w1 : p.tokens){
	    	try {
				p.s = p.c.createStatement();
				p.s.executeUpdate(insertUnigramToken(doc.fileName,w1));
				p.s.close();
				p.c.commit();				
			} catch (SQLException e) {
				e.printStackTrace();
				System.exit(0);
			}
	    }
	    System.out.println("Done.");
	    //get bigrams & trigrams
	    System.out.print("Getting bigrams and trigrams... ");
	    for(int i = 0;i < p.tokens.length-1; i++){
	    	String w1 = p.tokens[i];
	    	String w2 = p.tokens[i+1];
	    	
	    	String w3 = null;
	    	if(i < p.tokens.length-2)w3 = p.tokens[i+2];
	    	try {
				p.s = p.c.createStatement();
				p.s.executeUpdate(insertBigramToken(doc.fileName,w1,w2));
				if(w3 != null)p.s.executeUpdate(insertTrigramToken(doc.fileName,w1,w2,w3));
				p.s.close();
				p.c.commit();				
			} catch (SQLException e) {
				e.printStackTrace();
				System.exit(0);
			}
	    }	
	    System.out.println("Done.");
	    //clean bad ngrams	
	    System.out.print("Cleaning... ");
	    try {
	    	p.s = p.c.createStatement();
			String clean_unigrams = getCleaningQuery(doc,1);			
			p.s.executeUpdate(clean_unigrams);
			p.s.close();
	    	
			p.s = p.c.createStatement();
			String clean_bigrams = getCleaningQuery(doc,2);
			
			p.s.executeUpdate(clean_bigrams);
			p.s.close();
			
			p.s = p.c.createStatement();
			String clean_trigrams = getCleaningQuery(doc,3);
			
			p.s.executeUpdate(clean_trigrams);
			p.s.close();
			
			p.c.commit();
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	    System.out.println("Done.");
	    
	    
	}

	private static void aggregate(Parser p) {
		//aggregate
	    System.out.print("Aggregating... ");
	    try {
	    	p.s = p.c.createStatement();
			String aggregateUnigrams = "insert into unigrams_types_lenko (document,w1,freq) select document, w1, sum(1)"
					+ "from unigrams_tokens_clean_lenko "
					+ "group by document,w1 "
					+ ";";
			p.s.executeUpdate(aggregateUnigrams);
			p.s.close();
	    	
			p.s = p.c.createStatement();
			String aggregateBigrams = "insert into bigrams_types_lenko (document,w1,w2,freq) select document, w1, w2, sum(1)"
					+ "from bigrams_tokens_clean_lenko "
					+ "group by document,w1,w2 "
					+ ";";
			p.s.executeUpdate(aggregateBigrams);
			p.s.close();
			
			p.s = p.c.createStatement();
			String aggregateTrigrams = "insert into trigrams_types_lenko (document,w1,w2,w3,freq) select document, w1, w2, w3, sum(1)"
					+ "from trigrams_tokens_clean_lenko "
					+ "group by document,w1,w2,w3 "
					+ ";";
			p.s.executeUpdate(aggregateTrigrams);
			p.s.close();
			
			p.c.commit();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	    System.out.println("Done.");
	}

	private static String getCleaningQuery(Document doc, int n) throws Exception {
		StringBuilder sb = new StringBuilder();
		String ngram = "";
		if(n==1)ngram = "unigrams";
		else if(n==2)ngram = "bigrams";
		else if(n==3)ngram = "trigrams";
		else throw new Exception("Bad number of words.");
		sb.append("insert into " + ngram + "_tokens_clean_lenko select * from " + ngram + "_tokens_raw_lenko where ");
		for(int i = n;i>0;i--){
			for(String badToken : badTokens){
				sb.append("w" + i + " != '" + badToken + "' and ");
			}
		}
		sb.append("document = '" + doc.fileName +"';");
		return sb.toString();
	}
    
    private static String insertUnigramToken(String fileName, String w1) {
    	String word1 = w1.replace("'", "''");
    	return "insert into unigrams_tokens_raw_lenko (document,w1) values ('" + fileName + "','" + word1 + "');";
	}

	static String insertBigramToken(String f,String w1, String w2){
    	String word1 = w1.replace("'", "''");
    	String word2 = w2.replace("'", "''");
    	
    	return "insert into bigrams_tokens_raw_lenko (document,w1,w2) values ('" + f + "','" + word1 + "','" + word2 + "');";
    }
    
    static String insertTrigramToken(String f,String w1, String w2, String w3){
    	String word1 = w1.replace("'", "''");
    	String word2 = w2.replace("'", "''");
    	String word3 = w3.replace("'", "''");
    	return "insert into trigrams_tokens_raw_lenko (document,w1,w2,w3) values ('" + f + "','" + word1 + "','" + word2 + "','" + word3 + "');";
    }
    
    static String readFile(String path, Charset encoding)  throws IOException 
	{
		  byte[] encoded = Files.readAllBytes(Paths.get(path));
		  return new String(encoded, encoding);
	}
}
