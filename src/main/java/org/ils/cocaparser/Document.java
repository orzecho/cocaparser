package org.ils.cocaparser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Document {
	public String filePath;
	public String fileName;
	
	public Document(String f){
		this.filePath = f;
		Pattern lastSegment = Pattern.compile("^.*[\\/]([^\\/]+)\\.txt$");
        Matcher m = lastSegment.matcher(f);
        if(m.matches())this.fileName = m.group(1);
	}
	
	static public void main(String[] args){
		Document test = new Document("documents/1r/1r/u1a1fn01.txt");
		System.out.println(test.fileName);
	}
	
}
