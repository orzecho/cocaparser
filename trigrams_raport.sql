\insert into trigrams_raport_lenko (document,word1,word2,word3,freq_in_text,word1_freq,word2_freq,word3_freq,bigram1_freq,bigram2_freq,trigram_freq,in_coca,mean_freq,pmi) 
select document,w1,w2,w3,freq_in_text,w1_freq,w2_freq,w3_freq,b1_freq,b2_freq,t_freq,
case when t_freq is null then 0 else 1 end,
t_freq::double precision / 425::double precision,
trigram_pmi(cast(t_freq as bigint),w1_freq,w2_freq,w3_freq,b1_freq,b2_freq)
from trigrams_extended_lenko;

create function trigram_pmi(t_freq bigint, w1_freq bigint, w2_freq bigint, w3_freq bigint, b1_freq bigint, b2_freq bigint) returns double precision as $$
declare
	w123 numeric(65,8);
	b1b2 numeric(65,8);
	corpus constant numeric(65,8) := 425000000.0; 
begin
	w123 := cast(w1_freq as numeric(65,8)) * cast(w2_freq as numeric(65,8)) * cast(w3_freq as numeric(65,8));
	b1b2 := cast(b1_freq as numeric(65,8))*cast(w3_freq as numeric(65,8))+cast(w1_freq as numeric(65,8))*cast(b2_freq as numeric(65,8));
	return cast( cast(t_freq as numeric(65,8)) / ( w123 / power(corpus,2.0) + b1b2 / corpus ) as double precision );
end;
$$ language plpgsql;
								