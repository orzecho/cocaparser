truncate unigrams_tokens_raw_lenko;
truncate unigrams_tokens_clean_lenko;

truncate bigrams_tokens_raw_lenko;
truncate bigrams_tokens_clean_lenko;

truncate trigrams_tokens_raw_lenko;
truncate trigrams_tokens_clean_lenko;

truncate unigrams_types_lenko;
truncate bigrams_types_lenko;
truncate trigrams_types_lenko;

select setval('unigrams_types_lenko_id_seq',1);
select setval('bigrams_types_lenko_id_seq',1);
select setval('trigrams_types_lenko_id_seq',1);
select setval('bigrams_raport_lenko_id_seq',1);

truncate bigrams_raport_lenko;
truncate documents_pending_lenko;