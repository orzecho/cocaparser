﻿The use of mobile phones in Poland has grown considerably in the last few years. Polish market is trying to catch up with global trends, i.e. popularize mobile phones as much as possible and make them available for an average citizen. Thus various companies trying to attract clients, offer all kinds of promotions. Their goal has almost been achieved - the use of mobile phones in Poland became as wide as an Western markets.

There are far more supporters of this "fashion" that opponents. Mobile phone brings unquestionable advantages. First of all one can make or receive a phone call anytime and anywhere. This saves lots of time and 
effort. In some cases it turns out to be a real blessing. There are some professions whose success depends on immediate reaction, e.g. stock brokers. In emergency situations mobile phone can even save life.

These advantages, however, are often misused and then the mobile phones turn into a curse. The sound of ringing seems to be present - buses, restaurants or even theatres during a performance are no exceptions. That, of course, is not only unnecessary but also impolite, it annoys and disturbs.

It is hard to state whether mobile phone is a blessing or a curse. As long as it used only when necessary, it is undoubtedly one of the main inventions of the . century.

