﻿First cell phones were introduced to our disposition in late. Their major function was to make our life easier. What happened instead was over connectedness in people 's lives, one became easily reachable and thus frustration increased. However, it was a great impact on business-world. For those, who are often out of their office, a cell phone became a real treasure.

Even recent events point out that cell phones are really irreplaceable. After the attacks on World Trade Center in NYC, relatives and friends of victims could reach them by cell phones to make sure that everything was all right. There were calls from passengers of hijacked airplanes to loved ones to say good-bye. And, of course, using a cell phone, made working easier for firemen and the police, who were digging in tons of rubble. The only drawback was that because of overuse of cell phones, networks were jammed.

My personal experience showed that I just can not live without a cell phone. Not because of the fact that I am rarely at home. When my friends wanted to contact me they had to call my aunt and ask about my schedule, or leave a message for me. That was making her furious, because sometimes they did not want to introduce.

When I finally got my cell phone, it solved all problems. Now I stay in touch with all my friends and this helps to avoid all kinds of misunderstanding. I am happy and so is my aunt.

For those, who are looking at a cell phone as an intruder into a private life, I would recommend to read instructions carefully. You may always set it off and it will not be a nuisance any more. Only a good friend in a difficult situation.

Statistics also show, that using cell phones helps to decrease incidents. Being high in the mountains, or doing hiking is no more a problem to worry about. Just take your backpack and have fun. In case of incident you may always dial emergency numbers which are free. Of course, this is not our fault if an operator is unavailable.

So, I would say that cell phones and other technology are a gift from God. It only is required a little knowledge about purposes and exploitation of a cell phone, the slightest idea what is expected from this little thing.

