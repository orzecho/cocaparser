﻿Since the beginning of our civilization we very often have had to face many confusing questions. Each discovery is succeeded by discussions concerning pros and cons of the newest achievement. It is human inquisitive nature that make us think even doubt. We wonder if food we eat is nutritious enough, let alone mobile phones, considered to be one of the most powerful achievement of our time.

It is well known that in many everyday situations they come in very handy. However, one can ask "What about rays they produce?" It is out of question they are a real blessing for those who lead an active life, who are always on the run. But it must be remembered that for every advantage there is disadvantage.

For many years many surveys have taken place some of them seem to suggest that this blessing, in the long run, can prove to be a curse as they can cause brain cancer and by all accounts  they are considered to damage our brain tissue.

For the time being we are not able to observe any bad affect in our everyday life effects concerning our health. But if it is about our social life, I can point out lots of negative effects. Everywhere you go - to the cinema, theatre or even to a lecture, you are bound to hear a few different signals in the least appropriate moments. I try to be an understanding person and find justification for some aspects of our life, for some people it is simply a must to own a mobile phone, let us take doctors for instance. They are to be on the alert all the time. I maintain our mistake is to overuse this "tiny thing" which sometimes can even save our life. If it had not been for the cell phone, I would have myself walk a few kilometers home instead calling my father to come. There is no doubt, mobile phones make our lives easier. But on the other hand, where is the limit for this?

It must be mentioned, we leave in the age of not exactly mobile phones but "short messages". It is more and more common, especially for youths, not to talk but write. In this way we make our life far more comfortable but, in fact, day by day we lose ability to talk. Each day we seem to be less capable of communicating face to face the thing of high importance is that our language is our main feature which make us different from animals. For we must be aware of not losing it.

To sum up, I am really not able to say whether I am for or against mobile phones. Simply are a social phenomenon and it is hard to say whether pros overweigh cons. It depends on where we are, what kind of situations or hardship we are to face.


