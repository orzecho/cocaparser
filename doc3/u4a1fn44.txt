﻿The hay day of the mobile phones in Poland is over. This little device is no long a novelty for anyone. What is more, it seems that people stopped paying any attention to the fact that their lives looked simpler when they were not addicted to the usage of the cell phone.

It is symptomatic that in the modern society we tend to act as artistically damaged whenever we are acquainted with the latest technologies. The internet, television or even phone can pose a threat to the psyche of people.

Every usage of this device, taken to the extremes, causes addiction. That is why there are so many men and women in the streets who seem to never switch off their cells.

The fact makes also their attitudes as well as lifestyles change. They stay in touch with the rest of the world while they perform other action such as eating, queuing, shopping or even relaxing.

But what those mobile phone addicted people may not realize is the fact that this little device can sometimes cause a major problem. It very often happened that the telephone ringing spoiled a concert or a performance when the owner forgot to disconnect  it. Besides the endless talking on the phone makes it difficult to economize on the bills. Not to mention the radioactive waves that permeate through our brain and can cause cancer.

However, it is common knowledge that everything is made and created for the benefit of men and it is only up to them how they use the wealth of the world and technology.

Being absolutely against the invention a year ago, the argument above were totally convincing to me. However, not having a home number quickly made me change my mind. It was surprising for me to find out that a mobile phone is very convenient because it is small and can be carried everywhere. Whenever I needed to reach somebody in order to notify him or her that I would be late to a meeting, all I had to do was to push a couple of buttons without having to look for a telephone box.

It also allows me to stay in touch with my family as they can call me whenever they want to ask how I was feeling.

The mobile phone can also be of help whenever we find ourselves in a difficult situation, for example when we need to call a cab in the middle of the night in order to come back home safely. It could happen that we get lost in the mountain during a walk. Then the cell is highly beneficiary - it saves our lives.

The Short Messages System (SMS) that is connected with the mobile phones could also be useful when you can not talk or reach the person you want to get through to. It is also much cheaper than a conversation and you can use it when you want to send a message.

Thus, it all depends on the user if they make the device work most efficiently or they decide to throw out their money for pointless talks.

