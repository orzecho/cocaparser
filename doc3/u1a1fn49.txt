﻿We live in the century during which the world has almost become a global village. We have more and more possibilities to communicate with a person staying in a very distant country in only one minute. Such devices as Internet or mobile phone are of common use. But the arises one question: is a mobile phone really a blessing of a modern society ?

Nowadays when we are living our high-pace life mobile phones occur to be very useful. First of al., they provide us with the possibility to communicate with other people wherever we are. Only one call from a mobile phone in case of an accident may safe somebody 's life. Moreover, a mobile phone is a blessing for every businessman. It gives him an opportunity to contact his company at any time of the day. For instance, a stock broker may safe his finances from bankruptcy by making only one phone call.

What is more, mobile phones are very helpful to every ordinary man. They make it possible to contact your friends whenever you need it. If you, for instance somehow got lost, or you can not remember where the meeting point was, or even you want to inform somebody that you will be late there is nothing easier than to send them a short message. Apart from that, you also have the possibility to call a taxi or for somebody to pick you up if you want to come home late in the night.

Mobile phones are also a blessing for parents who are worried about their children. They can call them at any time of the day even if they are far away on holidays. However, the children may not always want their parents to control them so in that case they can switch their phone off.

Although somebody may find a mobile phone a real nuisance they can not question it is advantages. It is obvious that in modern world such devices as mobile phone are necessary and becoming more and more commonly used.


