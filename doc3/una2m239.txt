﻿The cell phone, is it good or bad for today 's society? Everywhere you look these days someone has a cell phone. Young, old and everyone in between owns a cell phone. People all over the world own cell phones. It is not just and American thing.

These mobile phones have many uses. Some people own them just in case of an emergency. Others use them almost the entire day. Answering calls or returning calls about their life and work. Many of America 's teenagers use their phones to contact friends. Some just use them to play games. However a person uses his/her cell phone there are many being used today.

Cell phones may be nice to have but can also be a nuisance to many people around them. Many times you and a friend may be having a conversation when you or your friends cell phones rings. This disrupts the conversation and leaves you hanging if you were talking about something important. Other times you may just be sitting outside and relaxing when the person next to you cell phone rings. This shakes you up a bit and makes it harder for you to relax again. When in a big city, such as Minneapolis, there are tons of people with cell phones. It seems like every minute someone 's phone is ringing. It gets to be somewhat annoying always hearing beeps and rings.

There are also good uses for cell phones. Many of today 's businessmen would be last without one. It is their means of communication between them and their clients, boss ' and co-workers. This even affects us. We could be the client that needs something soon, or the business man that needs to find out about something or someone before three o'clock.

Cell phones are also useful to us non-business people. We can use them to get a hold of friends to see what is happening that night. We can call our family if we are in need of something. On new cell phones people can even check the weather or check their e-mail. The list is practically endless of things you can do with a cell phone.

One of the best reasons for having a cell phone is in case of an emergency. There are many times when cell phones have come in useful. Many people have gotten into accidents but are able to call for help using their cell phone. If there is something happening while you are driving down the road you can call for help on your cell phone. I have heard of many stories, especially in our area of how someone was caught out in a blizzard and they used their phone to call for help or their phone was found by a tracking beam.

There are both good reasons and bad reasons for having a cell phone. I can handle listening to phones ring constantly when I know that that very phone could possibly help save me if I were in trouble. Cell phones are good to have in case of an emergency or just to get a hold of friends. Cell phones are a good thing for the future and will help to improve our lives.


