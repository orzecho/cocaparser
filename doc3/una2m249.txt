﻿

What am I supposed to do without my cell phone, is a line people often hear in the . Century. For many people, cell phones are a blessing, because they can call for directions or just talk to a distant relative. For other people cell phones are a curse, because with today 's technology, people can be traced using the only signal from there phone.

In the first case, many people use their cell phones in emergencies. They feel that having their cell phone makes them more prepared in any circumstance. These people could be in a car accident and phone for help or could have a loved one in need of medical assistance.

Other people have a cell phone for the simple reason of "everyone else has one". They feel, that to fit in today, they need to have a cell phone.

Today in the . century, children are growing up too fast. These children are walking around, talking to their little friends on their cell phones. Today 's children should not have to worry about making sure that there battery is fully charged, or if they have enough money to pay for their bill that month. Today 's children should only have to worry about, using their imagination, getting enough sleep, and eating there vegetables. For these reasons I feel that cell phones are a curse.

On the other hand cell phones can be used be everyday people with a hard schedule. These people may have many things going on in their life. This person may be a single parent with an infant and a full time job, who need the option of getting hold of someone without having to search for the nearest pay phone. This person could also be a big cooperate executive who needs to stay in contact with his clients. He might need to close a deal and using his cell phone he could get it done in the most efficient way possible.

Today with modern technology, people can be talking on their phone while someone else is listening in on their conversations or someone maybe trying to locate them from the cell phones signal. Many people think that by having a cell phone they can talk anywhere and time in privacy, but this might not be the case.

In conclusion, there is no clear-cut answer to the question "are cell phones a blessing or a curse at the end of the . century". There are too many conflicting cases and different circumstances.


