﻿By the end of the . century a mobile phone has become a device used by almost everyone in our small country. This instrument is a symbol of a kind of living standard. Of course most mobile phones are being bought by youngsters. It is very typical of a person to have his or her own cell phone. whether it is good or bad to have one is a matter worth discussing.

Working adults mostly use their phones in their workplace. I guess this idea is advisable, because they are quite easy to contact with and they keep in touch with their work and everything connected with it.

Apart from that if they have their children at home for example, they can easily contact them to make sure that everything is under control.

Of course every coin has two sides. If the situation is different, I mean when the child owns a mobile phone, the youngster uses his or her mobile phone to "show off". Most young people persuade their parents into buying a cell phone in case something happens. Their parents are mostly very naive and buy their poor baby a phone with a view to controlling him or her every time they want. They do not even imagine how wrong they are. It is quite easy to cheat the parent while talking through the mobile phone. If the child is clever enough the parent is unable to check where his beloved son or daughter are when talking.

Apart from that most mobile phones are unhealthy. The radio waves they produce are mostly radioactive. Those waves in most cases are capable of ruining your nervous system, especially the brain. They distract the brainwaves and in this way they can lead to brain cancer, which is nowadays incurable.

In my opinion mobile phones should not be totally banned. When you use it in a  proper way, this means using when it is necessary, they are advisable. In this way cell phones become a blessing for people working outside their workplace. But when it comes to buying such phones for kids or irresponsible people, who use their phones to show off and to play with them, I am against. The sounds mobile phones produce are a terrible nuisance for other people in public places.

Lastly I find mobile phones useful, but people should attach some cultural behavior to using them. The sad thing is such cultural usage of phones is not the case in Poland. I sincerely hope it will soon change for better.


