I you sit in a cinema and your neighbor just has to speak with his wife who left at home . or if you attend a Sunday mass and you hear loud (not church) bells in most solemn moments . then you probably feel like killing somebody - maybe the owner of highly specialized toy, maybe Mr Bell himself (however he is already happy in the second world).

But if the lift you are in has stuck between levels . or if you have lost in the mountains and it is getting darker and darker . then you are happy that you remembered to load your phone battery.

Mobile phone is like nuclear power: you can make it useful or you can use it to destroy (in this case-silence around us). As there are not stupid things - only irresponsible people using them.

I think about buying myself a mobile phone. I have got some good reasons. First one: prices. SMS is so much cheaper than even short talk through a traditional phone! (and you just can not take the last one with you everywhere) I often have something to communicate to my husband: it may be very important message about afternoon shopping or just the result of need to say "hello" (he gets up at . every morning and I am back home at about , so except weekends we have not many occasions to talk).

The second reason is professional: I write articles to "Mobile Internet" magazine - and I even have not a mobile phone! till now I have been helping myself somehow, but . Especially knowing how WAP works would help me to interview the authors of WAP sites.

Is a mobile phone a blessing or a curse? Why to think about it? A mobile phone is inevitable like Internet and television. Times change and we change with them. and, to be quite frank, I would rather telephone technology developed than we invested in war industry. However, the second is inevitable fac.


