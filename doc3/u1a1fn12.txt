﻿When over a hundred years ago . invented a telephone no one would expect it to become the greatest invention of the . century. In the second part of the century nobody would imagine its day without a telephone. And in the . the mobile phone came into usage.

At first it was strange to see people walking in the street and talking to their hand. Some said that mobile phones would never become popular. As we may see now they could not have been more wrong.

Today 's world of business, social life or just any other aspect of life would be much more difficult without a mobile phone.

First of all imagine yourself as a tourist. You visit a small village, where the civilization has not arrived yet. You have to make an urgent call but there is no phone around. What do you do? You take your mobile phone and call or send SMS. What is more, such situation may happen in the city as well. Let us face it, you may go along many streets and find no phone-box, or if you happen to find one, the phone will, all most certainly, be broken.

Then all sorts of emergency calls. If your car has broken down and you need help, use a mobile phone (as one of the companies says).

Finally, if you are a parent and your child happens to have a mobile phone, at any time you may check what it is doing, who it is with, whether it is safe or not. So it is a great advantage for a caring parent.

Some people would certainly mention phones ringing in the cinema or theatre, or in a classroom as an argument against using a cell phone. But I think that such situations are just a matter of good manners.

I, myself, do have a cell phone and I can not imagine my life without it. For me a mobile phone is definitely a blessing of the . century.


