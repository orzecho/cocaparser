﻿Unexpected sounds of a mobile phone well hidden in a bystander 's pocket at a funeral service will probably never cease to shock us. What might change, however, is the frequency of such occurrences as the cellular phone is steadily becoming another part of our daily inventory. If it is our phone that starts ringing at an unwanted time, we will gradually observe more and more of our friends or relatives make that hurried and embarrassed gesture towards their pockets or handbags.

Much has been argued in favor, as well as against mobile phones at it would be futile  to run through the list yet again. It might prove more interesting to see how we have changed because of them, if at all. Or, better still, for I may be wrong about the others, I will try to decide whether my reliable Nokia is a gift from God or Satan.

Most likely, as it often is, from neither. However, traces of addiction can surely be found in my treatment of the machine: hardly a day goes by without my using it.

Without it, I am at a loss: my friends ' phone numbers have all at once vanished from my memory, the phone booths are out of order and, worst of all, it is as if I have been somehow handicapped in the rat race for a day.

The question that comes to mind immediately is how on Earth I had managed without it before? Is the machine answering the needs I had no awareness of until now, or is it perhaps creating  them at my expense, both

Meanwhile, a lesser known composer has recently written symphony for . mobile phones. The spectators were asked for their numbers and subsequently a short time was sent to their apparatus, to be played in due time during the concert. It says more about mobile phones than my essay ever could: they are at most harmless can even be used to create art (of sorts).

To speak about spiritual impoverishment is as meaningless as it is trite. They are tools and it is only natural that they should be missed once we are left without them: after all we become easily accustomed to comfort. Long live mobile phones!

