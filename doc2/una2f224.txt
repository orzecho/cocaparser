

Cell phones, where would we be without them? They are so easy to use and not to mention convenient! Everyone loves chatting away no matter where they are or who is around. Some would even say that they are a blessing. well, I do not know if I would go that far.

There are so many times when you really need to call someone & do not have 
a phone, or even in case of an emergency, no matter what the call is cell phones can be helpful; but on the other hand, they can be the most overly used, annoying devices on the face of the earth! How many times have you been out to dinner or shopping or even at the movies and one of those annoying little rings pierces your ear drums & completely takes you from you train of thought? I see it happen consistently every day. Do not get me wrong, cell phones are a great way to communicate in case of an emergency or maybe you forgot something, but not for everyday conversation.

Just the other day I was shopping. Digging through the clothes racks & chatting with my mom, when I was suddenly bombarded with another conversation from across the room. I do not know what about you but I certainly do not want to hear about aunt . heart condition or . first day of school. I do not understand why people have such personal conversations while the vest of the world listens in! What do you talk about on your phone? Hopefully nothing you do not want others to know about.

Over all I will admit that cell phones are convenient and can be used well, but from my experiences they have been more on the annoying side. I guess I would not have anything against them if people did not use cell phones as if they could not function without that tiny device attached to their arm. Maybe all we need is to develop some sort of cell phone etiquette, so that the user can talk without disrupting every living being around them. Until the rules are set I guess we will just have to rely on people 's common sense, but even then we may be in trouble.


