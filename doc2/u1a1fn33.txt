The mobile phone is no longer the figment of our imagination. It is the reality.

This is the fruit of labor of the vast majority of engineers who strained their brains for a long time just to hit upon an idea of inventing a mobile phone. It is certain that the phone of this kind makes lives of torrents of people easier and more comfortable.

At first, some of us were reluctant, a little bit prejudiced  against using the device, which was quite expensive just after having been introduced.

But now gone are the days when using a mobile phone was hardly possible for an average person. Nowadays not only well-off people own them. The phones are cheaper and cheaper and, what in probably more important, people 's prejudice  against using them has disappeared.

The majority of us now is able to use them when being outside home. They are of great help not only to businessmen but also to less prominent members of social strata.

According to the latest data, students are more often than not the owners of the invention of the . century.

Yet, as I see it, the mobile phone is a real blessing of the . century. It is irreplaceable for people who have lots of duties to perform, to communicate with others.

We have got accustomed to their presence to such an extent that, I suppose, hardly any of us could do without them. The fact is that we have become addicted to them.

As regards becoming addicted, owning the mobile phone becomes less advisable (in my view, it is addiction that should be called the curse of the . century). You could possibly bear in mind that statement, be aware that no device is your God, that all addiction lead to family conflicts, which may even end up in a divorce.

My intention was not to exaggerate, but to say that everything used to a proper extend may be helpful. (according to statistics, people using mobile phones are more likely to suffer from heart attacks).


Whether the mobile phone becomes a blessing or a curse of the . century depends on your using it. One thing is certain, if  you do not want to feel alienated and you are keen on following the latest trends buy yourself a mobile phone.

Such a blessing should not hurt you.


