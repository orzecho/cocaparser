Nowadays everyone is familiar with the device called "mobile phone" since it has become very popular and many people use it in their everyday life. Some people say they would not be able to live and work without this small  thing whereas another group claims mobile phones cause so much trouble that they would be happy if it had never been invented. Which side is right? Is mobile phone a blessing or a curse of our times?

First of all, nobody can deny that the device is useful. It simply enables you to call anyone you want wherever you are. You do not have to look for a phone booth because you have got a telephone in your pocket.

Moreover, other people can call you and give you necessary information whenever there is a need for that. Consequently, it is obvious the mobile phone solves many problems concerning communication. It has therefore become an important tool of work for lots a people.

Furthermore, one reason why people do use mobile phones is that once you start to use it, it seems you would not be able to function without it. It is really hard to imagine someone who has been using a cellular phone suddenly gets rid of it.

Yet, there are a couple of reasons why you should not use a mobile phone. You must remember that this electronic device produces some kind of radiation which can do much harm to your heart.

Another thing is that mobile phones, which can start ringing everywhere just out of the blue, are not welcome in some places e.g. churches or theatres. Users of cellular phones seem not to be aware of the fact that their "useful tools of work" may be disturbing for other people.

Last but not least, there is one vice of the device which concerns its user only. A mobile phone tends to make its owner always alert. Imagine you have a cell while on holidays. You will get calls from people you left in your home town and you will not be able to relax. You can always turn the phone off but you might also forget to do so.

On balance, I think that mobile phone is very useful and might sometimes be a real blessing. However, people usually overuse the device, which makes mobile also a curse of our times.


