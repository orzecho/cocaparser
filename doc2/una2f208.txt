Mobile phones help many people every day. People use their mobile phones to help them or others out in emergency situations. Another way people use their mobile phone is to announce news to family and friends. Most people use their mobile phones just for general communication. Basically, mobile phones help in emergency situations, general communication, and telling people news.

Mobile phones are very convenient when it comes to emergency situations. When a person gets into a car accident they can use their mobile phone to call for help. If somebody got stuck in a snowstorm they could use their mobile phone to call for help. A person could also use their phone to call a loved one from a plane that has been hijacked.

Mobile phones can also be used to announce news to family and friends. A couple could use their mobile phone to call their family to announce the birth of a new baby. When kids are going to be late for their curfew they could call home to let their parents know. Sometimes parents can not make it to their child 's basketball game, so on the way home they could call to let them know how it went.

Mobile phones are mostly used for general communication. Most mobile phones have free long distance and it can be used to keep in contact with family. They can also be used to call a family member and remind them to get something from. Mobile phones can also be used to let someone know that you will be late for an appointment.

Mobile phones are good because they help out a lot of people each day. They help people out in emergency situations. Mobile phones can help to announce news to family and friends faster than waiting for another phone. Mobile phones help in emergency situations, general communication and telling people news. 


