select dl.group_id || ' col', count(*), avg(pmi) as avgpmi, avg(tscore) as avgtscore from typebigramswithdocuments as tbwd 
inner join bigram_raport as tb on tb.word1 = tbwd.word1 and tb.word2 = tbwd.word2 and tb.clips_post1 = tbwd.post1 and tb.clips_post2 = tbwd.post2 
inner join documents_lenko as dl on dl.document = tbwd.id_document 
where tbwd.post1 = 'JJ' and tbwd.post2 like 'NN%' and tb.pmi > 3 and tb.tscore > 2 group by dl.group_id
union
select dl.group_id || ' all', count(*), avg(pmi) as avgpmi, avg(tscore) as avgtscore from typebigramswithdocuments as tbwd 
inner join bigram_raport as tb on tb.word1 = tbwd.word1 and tb.word2 = tbwd.word2 and tb.clips_post1 = tbwd.post1 and tb.clips_post2 = tbwd.post2 
inner join documents_lenko as dl on dl.document = tbwd.id_document 
group by dl.group_id
;

select dl.group_id || ' col', count(*), avg(pmi) as avgpmi, avg(tscore) as avgtscore from typebigramswithdocuments as tbwd 
inner join bigram_raport as tb on tb.word1 = tbwd.word1 and tb.word2 = tbwd.word2 
inner join documents_lenko as dl on dl.document = tbwd.id_document 
where tbwd.post1 like 'NN%' and tbwd.post2 like 'NN%' and tb.pmi > 3 and tb.tscore > 2 group by dl.group_id
union
select dl.group_id || ' all', count(*), avg(pmi) as avgpmi, avg(tscore) as avgtscore from typebigramswithdocuments as tbwd 
inner join bigram_raport as tb on tb.word1 = tbwd.word1 and tb.word2 = tbwd.word2 
inner join documents_lenko as dl on dl.document = tbwd.id_document 
group by dl.group_id
;
